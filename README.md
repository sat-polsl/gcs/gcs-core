# GCS-Core

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/sat-polsl/gcs/gcs-core?branch=main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sat-polsl/gcs/gcs-core?)](https://goreportcard.com/report/gitlab.com/sat-polsl/gcs/gcs-core)
[![codecov](https://codecov.io/gl/sat-polsl:gcs/gcs-core/branch/main/graph/badge.svg?token=ZnpFOit210)](https://codecov.io/gl/sat-polsl:gcs/gcs-core)
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/sat-polsl/gcs/gcs-core.svg)](https://pkg.go.dev/gitlab.com/sat-polsl/gcs/gcs-core)

# Settings

All settings are set through environmental variables.

- [Base Configuration](#settings-base-configuration)
- [Storage](#settings-storage)
    - [File](#settings-storage-file)
    - [PostgreSQL](#settings-storage-postgresql)
- [SMTP](#settings-smtp)
- [Mocking](#settings-mocking)
- [Logging](#settings-logging)

## Settings: Base Configuration

- `PORXY_ENDPOINT`
    - Address of the host of GCS-Proxy,
    - **Type**: `string`
    - **Default value**: `localhost`
- `PUB_PORT`
    - Publisher port of the GCS-Proxy,
    - **Type**: `string`
    - **Default value**: `14789`
- `SUB_PORT`
    - Subscriber port of the GCS-Proxy,
    - **Type**: `string`
    - **Default value**: `14790`
- `DOCKER_TIMEOUT`
    - Docker REST API timeout,
    - **Type**: `string`
    - **Default value**: `45s`
- `ROTOR_LONGITUDE`
    - Position of rotor (Longitude),
    - **Type**: `float`
    - **Default value**: `18.682064`
- `ROTOR_LATITUDE`
    - Position of rotor (Latitude),
    - **Type**: `float`
    - **Default value**: `50.293436`
- `ROTOR_ALTITUDE`
    - Position of rotor (Altitude),
    - **Type**: `float`
    - **Default value**: `231.0`
- `MODULES`
    - Indicates which modules should be active,
    - **Default value**: (*empty*)
    - **Separator**: `:`
    - **Possible values**:
        - `ZMQ`
            - **Default value**: `true`
    - **Example**: `MODULES=ZMQ=true:MODULE2=true:MODULE3=false`
- `INSECURE`
    - Indicates if program should check incoming requests for valid API keys,
    - **Type**: *bool*
    - **Default value**: `false`

## Settings: Storage

- `VAULT_TOKEN`
    - Indicates if program should ferch database credentials from vault, and specifies vault token,
    - **Type**: *string*
    - **Default value**: (*empty*)
- `VAULT_ADDRESS`
    - Indicates the address of Vault,
    - **Type**: *string*
    - **Default value**: `vault`

### Settings: Storage: File

- `DATA_FILES_EXTENSION`
    - Indicates if the content should be dumped to log stream in case of writing error,
    - **Type**: *string*
    - **Default value**: `txt`
- `DATA_FILES_LOCATION`
    - Indicates if the content should be dumped to log stream in case of writing error,
    - **Type**: *string*
    - **Default value**: `data_dump`
- `DUMP_FILE_SHARD_SIZE`
    - Size above which new shard of dump file is created (in Megabytes). Final size of file may be bigger, as the counting of size does not take padding and "prettifying" into account,
    - **Type**: *int64*
    - **Default value**: `4`
- `EMERGENCY_CONTENT_DUMP`
    - Indicates if the content should be dumped to log stream in case of writing error,
    - **Type**: *bool*
    - **Default value**: `false`

### Settings: Storage: PostgreSQL

- `POSTGRES_HOST`
    - Database name if using PostgreSQL as storage
    - **Type**: *string*
    - **Default value**: `postgres`
- `POSTGRES_PORT`
    - Database name if using PostgreSQL as storage
    - **Type**: *unsigned int*
    - **Default value**: `5432`
- `POSTGRES_DATABASE`
    - Database port if using PostgreSQL as storage
    - **Type**: *string*
    - **Default value**: `sat_dev`
- `POSTGRES_USERNAME`
    - Username used to authenticate to PostgreSQL, not used when using Vault,
    - **Type**: *string*
    - **Default value**: (*empty*)
- `POSTGRES_PASSWORD`
    - Password used to authenticate to PostgreSQL, not used when using Vault,
    - **Type**: *string*
    - **Default value**: (*empty*)

## Settings: SMTP

- `SMTP_SECURE`
    - Indicates if the SMTP should use TLS,
    - **Type**: *bool*
    - **Default value**: (*empty*)
- `SMTP_EMAIL`
    - Email used to send auth tokens to the users,
    - **Type**: *string*
    - **Default value**: (*empty*)
- `SMTP_USER`
    - Username used to authenticate to the SMTP server,
    - **Type**: *string*
    - **Default value**: (*defaults to the SMTP_EMAIL*)
- `SMTP_PASSWORD`
    - Password used for email authentication,
    - **Type**: *string*
    - **Default value**: (*empty*)
- `SMTP_HOST`
    - SMTP hostname used for sending emails,
    - **Type**: *string*
    - **Default value**: `smtp.gmail.com`
- `SMTP_PORT`
    - SMTP port used for sending emails,
    - **Type**: *int*
    - **Default value**: `587`
- `ALLOWED_DOMAINS`
    - Indicates allowed domains for authentication,
    - **Type**: *string* (might be multiple strings, comma separated)
    - **Default value**: `polsl.pl`

### Settings: Mocking

- `MOCK_DOCKER_CTL`
    - Indicates if program should mock Docker controrller,
    - **Type**: *bool*
    - **Default value**: `false`
- `MOCK_ROTOR_CTL`
    - Indicates if program should mock rotor controrller,
    - **Type**: *bool*
    - **Default value**: `false`

## Settings: Logging

- `LOG_FORMAT`
    - Indicates the log format,
    - **Type**: *string*
    - **Default value**: (*empty*)
    - **Possible values**: `JSON`
- `LOG_LEVEL`
    - Indicates the log level,
    - **Type**: *string*
    - **Default value**: `info`
    - **Possible values**: `panic`, `fatal`, `error`, `warning`, `info`, `debug`, `trace`

# Build and run

1. Build: `docker compose build`
2. Run: `docker compose up -d`

# Useful links

- [G Go version manager](https://github.com/stefanmaric/g)
