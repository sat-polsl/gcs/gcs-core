FROM golang:latest AS build

COPY . /tmp/core/
WORKDIR /tmp/core/

RUN make mod build

FROM alpine:latest AS runtime

RUN apk add --no-cache libc6-compat

COPY --from=build /tmp/core/build/gcs-core /opt/gcs/core

RUN chmod -R 777 /opt/gcs/

ENV LOG_LEVEL=trace
ENV LOG_FORMAT=JSON

LABEL maintainer="mateusz.urbanek.98@gmail.com" \
    type="gcs-core"

ENTRYPOINT [ "/opt/gcs/core" ]
