api-key = "SUPER-SECRET-KEY"
email = "mail@example.com"

api-server {
    address = "localhost"
    port = 8080
    insecure = true
}

log-config {
    level = "info"
    format = "text"
}
