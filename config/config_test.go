package config_test

import (
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/config"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/test"
)

const (
	apiKey = "SUPER-SECRET-KEY"
	email  = "mail@example.com"

	apiServerAddress  = "localhost"
	apiServerPort     = 8080
	apiServerInsecure = true

	logConfigLevel  = "info"
	logConfigFormat = "text"
)

func TestLoad(t *testing.T) {
	err := config.Load("./test/example.hcl")
	if err != nil {
		t.Errorf("loading config failed: %s", err.Error())
	}

	cfg := config.Get()

	test.Assert(t, cfg.APIKey, apiKey)
	test.Assert(t, cfg.Email, email)

	if cfg.Server != nil {
		test.Assert(t, cfg.Server.Address, apiServerAddress)
		test.Assert(t, cfg.Server.Port, apiServerPort)
		test.Assert(t, cfg.Server.Insecure, apiServerInsecure)
	} else {
		t.Errorf("config.Server is nil")
	}

	if cfg.Logger != nil {
		test.Assert(t, cfg.Logger.Format, logConfigFormat)
		test.Assert(t, cfg.Logger.Level, logConfigLevel)
	} else {
		t.Errorf("config.Logger is nil")
	}

	err = config.Load("./test/nil-example.hcl")
	if err != nil {
		t.Errorf("loading config failed: %s", err.Error())
	}

	cfg = config.Get()

	if cfg.Server != nil {
		t.Errorf("config.Server is not nil: %v", cfg.Server)
	}

	if cfg.Logger != nil {
		t.Errorf("config.Logger is not nil: %v", cfg.Logger)
	}
}
