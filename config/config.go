// Package config is used to read and store client configuration saved in .hcl
// format.
package config

import (
	"errors"
	"fmt"
	"os"
	"path"

	"github.com/hashicorp/hcl/v2/hclsimple"

	"gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type Config struct {
	Server *Server         `hcl:"api-server,block"`
	APIKey string          `hcl:"api-key"`
	Email  string          `hcl:"email"`
	Logger *logger.Options `hcl:"log-config,block"`
}

type Server struct {
	Address  string `hcl:"address"`
	Port     int    `hcl:"port"`
	Insecure bool   `hcl:"insecure"`
}

var config Config

func Get() *Config {
	return &config
}

func Load(file string) error {
	// verify if file is in any context
	_, err := os.Stat(file)
	if err == nil {
		return load(file)
	} else if errors.Is(err, os.ErrNotExist) {
		home, err := os.UserHomeDir()
		if err != nil {
			return fmt.Errorf("unable to get user's home dir: %w", err)
		}

		fileAtHome := path.Join(home, file)
		logger.Log().Debugln()
		_, err = os.Stat(fileAtHome)
		if err == nil {
			return load(fileAtHome)
		}
	}

	return nil
}

func load(file string) error {
	// ensure that no previous values are preserved
	config = Config{}

	if err := hclsimple.DecodeFile(file, nil, &config); err != nil {
		return fmt.Errorf("unable to decode config file: %w", err)
	}

	logger.Log().Tracef("%+v\n", config)

	return nil
}
