include .build.env
include .dev.env

.PHONY: all
all: clean build

.PHONY: build
build:
	$(BUILD_OPTIONS) go build -o build/$(EXECUTABLE_NAME) main.go

.PHONY: clean
clean:
	rm -rf build/

.PHONY: doc
doc:
	pkill godoc
	[ ! -d "tmp" ] && mkdir tmp || echo "ok"
	nohup godoc -http=:8080 > tmp/doc.log 2> tmp/doc.err &
	echo "http://localhost:8080/pkg/gitlab.com/sat-polsl/gcs/gcs-core/"

.PHONY: mod
mod:
	go mod download
	go mod tidy

.PHONY: docker
docker:
	docker buildx build --platform linux/amd64,linux/arm64 --output=type=registry -t $(IMAGE_TAG) .

.PHONY: run
run:
	go build -o build/$(shell go env GOOS)/$(shell go env GOARCH)/$(EXECUTABLE_NAME) main.go
	$(CORE_SETTINGS) ./build/$(shell go env GOOS)/$(shell go env GOARCH)/$(EXECUTABLE_NAME)

.PHONY: test
test:
	go clean -testcache
	$(TEST_SETTINGS) go test -cover -race ./...

.PHONY: compose-build
compose-build:
	docker compose build --no-cache

.PHONY: hooks
hooks:
	pre-commit install --hook-type pre-commit
	pre-commit install --hook-type commit-msg

.PHONY: start-dev
start-dev:
	cd .dev && docker compose up --detach

.PHONY: stop-dev
stop-dev:
	cd .dev && docker compose down
