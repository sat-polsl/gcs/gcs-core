package logger

const (
	EnvLogFormat = "LOG_FORMAT"
	EnvLogLevel  = "LOG_LEVEL"
)

const (
	From = "from"
	At   = "at"
)
