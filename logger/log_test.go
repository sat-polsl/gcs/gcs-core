package logger_test

import (
	"reflect"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

func TestConfigLogger(t *testing.T) {
	logger.ConfigLogger(logger.Options{
		Format: "json",
		Level:  "fatal",
	})

	if lvl := logger.Log().GetLevel(); lvl != logrus.FatalLevel {
		t.Errorf("got: %v, wanted: %v", lvl, logrus.FatalLevel)
	}

	got := reflect.TypeOf(logger.Logger.Formatter)
	wanted := reflect.TypeOf(&logrus.JSONFormatter{})
	if got != wanted {
		t.Errorf("got: %v, wanted: %v", got, wanted)
	}
}
