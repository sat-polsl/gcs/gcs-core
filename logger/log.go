// Package logger contains default logrus configuration.
package logger

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

// Logger is default instance of logger used in all other packages instead of
// global scope logrus.Logger.
var Logger *logrus.Logger

var (
	cancel context.CancelFunc
	RC     int
)

func init() {
	Logger = logrus.New()

	configLogger()
}

type Options struct {
	Format string `hcl:"format"`
	Level  string `hcl:"level"`
}

func ConfigLogger(opt Options) {
	if opt.Format == "json" {
		Log().SetFormatter(&logrus.JSONFormatter{})
	} else if opt.Format == "text" {
		Log().SetFormatter(&logrus.TextFormatter{})
	}

	switch opt.Level {
	case logrus.PanicLevel.String():
		Log().SetLevel(logrus.PanicLevel)

	case logrus.FatalLevel.String():
		Log().SetLevel(logrus.FatalLevel)

	case logrus.ErrorLevel.String():
		Log().SetLevel(logrus.ErrorLevel)

	case logrus.WarnLevel.String():
		Log().SetLevel(logrus.WarnLevel)

	case logrus.InfoLevel.String():
		Log().SetLevel(logrus.InfoLevel)

	case logrus.DebugLevel.String():
		Log().SetLevel(logrus.DebugLevel)

	case logrus.TraceLevel.String():
		Log().SetLevel(logrus.TraceLevel)
	}

	Log().WithField(At, "configLogger").Debugf("log level set to: %s", Log().GetLevel().String())
}

func configLogger() {
	if os.Getenv(EnvLogFormat) == "JSON" {
		Log().SetFormatter(&logrus.JSONFormatter{})
	}

	switch os.Getenv(EnvLogLevel) {
	case logrus.PanicLevel.String():
		Log().SetLevel(logrus.PanicLevel)

	case logrus.FatalLevel.String():
		Log().SetLevel(logrus.FatalLevel)

	case logrus.ErrorLevel.String():
		Log().SetLevel(logrus.ErrorLevel)

	case logrus.WarnLevel.String():
		Log().SetLevel(logrus.WarnLevel)

	case logrus.InfoLevel.String():
		Log().SetLevel(logrus.InfoLevel)

	case logrus.DebugLevel.String():
		Log().SetLevel(logrus.DebugLevel)

	case logrus.TraceLevel.String():
		Log().SetLevel(logrus.TraceLevel)
	}

	Log().Infof("level set to: %s", Log().GetLevel().String())
}

// Log returns the default Logger.
func Log() *logrus.Logger {
	return Logger
}

// Anonimize changes visible characters in content, so it can also be displayed
// without being compromised.
func Anonimize(content string, visible int, anonimized int) string {
	if len(content) > visible+anonimized {
		return fmt.Sprintf("%s%s", content[0:visible], strings.Repeat("x", anonimized))
	}
	Log().WithField(From, "anonimize").Warnln("anonimizing full length, as the len(content) <= visible+anonimized")
	return strings.Repeat("x", visible+anonimized)
}

func SetCancel(c context.CancelFunc) {
	cancel = c
}

func Fatal() {
	if cancel != nil {
		cancel()
	}

	RC = 255
}
