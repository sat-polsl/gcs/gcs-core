package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/advenv"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/io"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/notifier"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/rotor"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/server"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

var (
	mutex sync.Mutex
)

func wrap(ctx context.Context, cancel context.CancelFunc, wg *sync.WaitGroup, t threads.Starter, ipc *threads.IPC) {
	wg.Add(1)
	l.Log().Infof("starting thread: %s", t)
	if err := t.Start(ctx, wg, ipc); err != nil {
		defer cancel()

		l.Log().Error(err)

		mutex.Lock()
		defer mutex.Unlock()
		l.RC++
	}
}

func hb() {
	for {
		l.Log().Trace("running...")
		time.Sleep(time.Second * 30)
	}
}

func main() {
	l.Log().Infof("PID: %v", os.Getpid())

	module := advenv.Parse("MODULES")

	go hb()

	ipc := threads.New()
	if _, err := ipc.New(threads.SubRotorChannel); err != nil {
		l.Log().Fatalf("unable to create new IPC channel: %s", err.Error())
	}
	if _, err := ipc.New(threads.PubRotorChanel); err != nil {
		l.Log().Fatalf("unable to create new IPC channel: %s", err.Error())
	}
	if _, err := ipc.New(threads.StorageChannel); err != nil {
		l.Log().Fatalf("unable to create new IPC channel: %s", err.Error())
	}
	if _, err := ipc.New(threads.CriticalNotificationChanel); err != nil {
		l.Log().Fatalf("unable to create new IPC channel: %s", err.Error())
	}

	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(context.Background())
	l.SetCancel(cancel)

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGQUIT)

		<-c
		cancel()
		l.Log().Info("received signal notification, closing...")
	}()

	if m := module["ZMQ"].V; m != nil && m.(bool) {
		go wrap(ctx, cancel, &wg, io.NewPublisher(), ipc)
	}

	if m := module["ZMQ"].V; m != nil && m.(bool) {
		go wrap(ctx, cancel, &wg, io.NewSubscriber(), ipc)
	}

	n := notifier.New()
	go wrap(ctx, cancel, &wg, n, ipc)

	d, err := server.New()
	if err != nil {
		l.Log().Fatalf("unable to create docker controller: %s", err.Error())
	}
	go wrap(ctx, cancel, &wg, d, nil)

	go wrap(ctx, cancel, &wg, rotor.New(), ipc)

	s, err := storage.New()
	if err != nil {
		l.Log().Fatalf("unable to create storage controller: %s", err.Error())
	}
	go wrap(ctx, cancel, &wg, s, ipc)

	time.Sleep(10 * time.Millisecond)
	wg.Wait()

	mutex.Lock()
	defer mutex.Unlock()
	os.Exit(l.RC)
}
