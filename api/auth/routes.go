package auth

const (
	RouteBase     = "/auth"
	RouteRegister = "/register" // extends RouteBase
	RouteRemove   = "/remove"   // extends RouteBase
	RoutePrune    = "/prune"    // extends RouteBase
)
