// Package auth holds API definition (for both Server and Client) for
// Authorization endpoint.
package auth

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
	"gitlab.com/sat-polsl/gcs/gcs-core/mail"
)

type API struct {
	client mail.Client
}

func New() (*API, error) {
	return &API{client: mail.NewClient()}, nil
}

func (a API) Close() error {
	// TODO: Implement
	return nil
}

func (a API) Register(mux *mux.Router) {
	tools.RegisterGet(mux, RouteBase, a.CountKeys)
	authMux := mux.PathPrefix(RouteBase).Subrouter()

	tools.RegisterPost(authMux, RouteRegister, a.RequestAPIKey)
	tools.RegisterPost(authMux, RouteRemove, a.RemoveAPIKey)
	tools.RegisterPost(authMux, RoutePrune, a.PruneAPIKeys)
}

func (a API) CountKeys(w http.ResponseWriter, r *http.Request) {
	// TODO: Implement
}

func (a API) RequestAPIKey(w http.ResponseWriter, r *http.Request) {
	// ctx, cancel := context.WithTimeout(context.Background(),
	// containers.DefaultTimeout) defer cancel()

	var err error

	email := r.PostFormValue(FormEmail)
	if len(email) < 1 {
		tools.WriteError(w, r, http.StatusBadRequest, fmt.Errorf("mail address not provided"))
		return
	}

	if !a.client.DomainOk(email) {
		tools.WriteError(w, r, http.StatusBadRequest, fmt.Errorf("domain is not in the list of supported domains"))
		return
	}

	key := "hello"
	defer func() {
		if err == nil {
			// storage.AddAPIKey(email, key)
			l.Log().WithField(l.From, "request-api-key").Debugln("key written")
		}
	}()

	if err = a.client.SendAPIKey(email, key); err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusAccepted, tools.ResponseMap{
		"valid": true,
	})
}

func (a API) RemoveAPIKey(w http.ResponseWriter, r *http.Request) {
	// TODO: Implement
}

func (a API) PruneAPIKeys(w http.ResponseWriter, r *http.Request) {
	// TODO: Implement
}
