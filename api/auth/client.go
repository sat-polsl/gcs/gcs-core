package auth

import (
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/common"
	"gitlab.com/sat-polsl/gcs/gcs-core/config"
)

type ClientOptions struct {
	Protocol      string
	Client        *http.Client
	RouteRoot     string
	ServerAddress string
	ServerPort    string
}

func (opts *ClientOptions) Parse() ClientOptions {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return *opts
}

type Auth struct {
	client    *http.Client
	routeRoot string
	target    string
}

func NewClient(opts ClientOptions) Auth {
	return Auth{
		client:    opts.Client,
		routeRoot: opts.RouteRoot,
		target:    fmt.Sprintf("%s://%s%s", opts.Protocol, opts.ServerAddress, opts.ServerPort),
	}
}

func (a Auth) Register() (r common.Response, err error) {
	form := url.Values{}
	form.Add("email", config.Get().Email)

	method := http.MethodPost
	route := a.target + path.Join(a.routeRoot, RouteBase, RouteRemove)

	req, err := common.CreateRequest(method, route, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}

	r, err = common.ParseResponse(a.client.Do(req))

	return
}

func (a Auth) Remove() (r common.Response, err error) {
	method := http.MethodPost
	route := a.target + path.Join(a.routeRoot, RouteBase, RouteRemove)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(a.client.Do(req))

	return
}

func (a Auth) Prune() (r common.Response, err error) {
	method := http.MethodPost
	route := a.target + path.Join(a.routeRoot, RouteBase, RouteRemove)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(a.client.Do(req))

	return
}
