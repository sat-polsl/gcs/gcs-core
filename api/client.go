package api

import (
	"fmt"
	"net/http"
	"path"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/auth"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/containers"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/services"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/storage"
)

type Client struct {
	Address string
	Port    string
	Token   string

	apiBaseURL string

	httpClient *http.Client

	Containers containers.Containers
	Services   services.Services
	Auth       auth.Auth
	Storage    storage.Storage
}

var client *Client

func InitDefaultClient(address string, port int) {
	client = NewClient(address, fmt.Sprintf(":%d", port))
}

func Get() *Client {
	return client
}

func NewClient(address, port string) *Client {
	c := &http.Client{}

	return &Client{
		httpClient: c,

		apiBaseURL: path.Join(RouteBase, RouteVersion),

		Containers: containers.NewClient(containers.ClientOpts{
			Client:        c,
			RouteRoot:     path.Join(RouteBase, RouteVersion),
			ServerAddress: address,
			ServerPort:    port,
		}),

		Services: services.NewClient(services.ClientOpts{
			Client:        c,
			RouteRoot:     path.Join(RouteBase, RouteVersion),
			ServerAddress: address,
			ServerPort:    port,
		}),

		Auth: auth.NewClient(auth.ClientOptions{
			Client:        c,
			RouteRoot:     path.Join(RouteBase, RouteVersion),
			ServerAddress: address,
			ServerPort:    port,
		}),

		Storage: storage.NewClient(storage.ClientOpts{
			Client:        c,
			RouteRoot:     path.Join(RouteBase, RouteVersion),
			ServerAddress: address,
			ServerPort:    port,
		}),
	}
}
