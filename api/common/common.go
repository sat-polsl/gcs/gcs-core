// Package common contains structures, functions and variables used across
// multiple API subpackages.
package common

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/sat-polsl/gcs/gcs-core/auth"
	"gitlab.com/sat-polsl/gcs/gcs-core/config"
)

type Response struct {
	StatusCode    int
	StatusMessage string
	Content       map[string]interface{}
}

func CreateRequest(method string, url string, body io.Reader) (req *http.Request, err error) {
	if req, err = http.NewRequest(method, url, body); err != nil {
		return
	}

	req.Header.Add(auth.TokenHeader, config.Get().APIKey)

	if body != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	return
}

func ParseResponse(res *http.Response, inputError error) (r Response, err error) {
	if inputError != nil {
		return r, inputError
	}
	defer res.Body.Close()

	r.StatusCode = res.StatusCode
	r.StatusMessage = res.Status

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(b, &r.Content)
	if err != nil {
		return
	}

	return
}

func ParseCSVResponse(res *http.Response, inputError error) (r Response, err error) {
	if inputError != nil {
		return r, inputError
	}
	defer res.Body.Close()

	r.StatusCode = res.StatusCode
	r.StatusMessage = res.Status

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return
	}

	r.Content = map[string]interface{}{
		"csv": string(b),
	}

	return
}
