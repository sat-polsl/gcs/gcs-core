**Table of contents:**

- [Full routes](#full-routes)
- [Errors](#errors)
- [Secure mode](#secure-mode)
- [auth](#auth) <b label="todo" style="color:red">WARNING! Not implemented!</b>
  - [auth/register](#authregister)
  - [auth/remove](#authremove)
  - [auth/prune](#authprune)
- [containers](#containers)
  - [containers/{id}](#containersid)
    - [containers/{id}/kill](#containersidkill)
    - [containers/{id}/start](#containersidstart)
    - [containers/{id}/stop](#containersidstop)
- [services](#services)
  - [services/{name}](#servicesname)
    - [services/{name}/start](#servicesnamestart)
    - [services/{name}/stop](#servicesnamestop)
- [storage](#storage) <b label="todo" style="color:red">WARNING! Not implemented!</b>
  - [storage/{id}](#storageid)
    - [storage/{id}/info](#storageid)

## Full routes

Every route described in the header should be used according to the following pattern:

```
[address]/api/{version}/{route}
```

For example:

```
127.0.0.1:8080/api/v1/services/1/kill
```

## Errors

Every request can return error. Besides standard response code, the body of the repsonse can contain additional error info, for example:

```json
{
  "error": "error on list containers request: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?"
}
```

## Secure mode

When the Core service is started with the secure mode, every request must contain authentication token. To achieve this, an additional header must be added to the request.

You can achive it in following way using Go:

```go
req, err := http.NewRequest("GET", "127.0.0.1:8080/api/v1/services", nil)
if err != nil {
    panic(err)
}

req.Header.Add("X-GCSCore-Token", "<Token-generated-by-auth-endpoint>")
```


Or with Python:

```python
headers = {'X-GCSCore-Token': '<Token-generated-by-auth-endpoint>'}

r = requests.get(url, headers=headers)
```

## `auth`

This is endpoint used for authentication management. It is used to register (create new) users.

- Full route: `/api/{version}/auth`

### `auth/register`

<b label="todo" style="color:red">WARNING! Not implemented!</b>

Register endpoint is used for registering email for usage in the API with secure mode.

- Full route: `/api/{version}/auth/register`
- Method: **POST**
- Required content: **form**
- Content example:

```form
email: name.surname@example.com
```

- Response example:

```json
{
    "valid": True
}
```

### `auth/remove`

<b label="todo" style="color:red">WARNING! Not implemented!</b>

Remove endpoint is used for removing email/access token from usage in the API with secure mode.

- Full route: `/api/{version}/auth/remove`
- Method: **POST**
- Required content: **form**
- Content example: *TODO*
- Response example: *TODO*

### `auth/prune`

- Full route: `/api/{version}/auth/prune`


<b label="todo" style="color:red">WARNING! Not implemented!</b>

Remove endpoint is used for removing all emails/access tokens from usage in the API with secure mode.

- Full route: `/api/{version}/auth/remove`
- Method: **POST**
- Required content: **form**
- Content example: *TODO*
- Response example: *TODO*

## `containers`

Primitive used for the communication with the Docker API. By default lists all containers that can be accessed on host.

- Full route: `/api/{version}/containers`
- Method: **POST**
- Required content: *none*
- Response example:

```json
{
  "containers": [
    {
      "Id": "54d06fbd343c0b6656aeee0e1200145ba7770906b7aa1fd3d371c451f0222dc1",
      "Names": [
        "/earthly-buildkitd"
      ],
      "Image": "docker.io/earthly/buildkitd:v0.6.7",
      "ImageID": "sha256:51faa4bd51b8e341e2f685061c4d58c1e91ffbc40c34ee5141ea32cac623522d",
      "Command": "/usr/bin/entrypoint.sh buildkitd --config=/etc/buildkitd.toml",
      "Created": 1644762488,
      "Ports": [],
      "Labels": {
        "dev.earthly.settingshash": "c84852888d8b51c2"
      },
      "State": "exited",
      "Status": "Exited (137) 7 days ago",
      "HostConfig": {
        "NetworkMode": "default"
      },
      "NetworkSettings": {
        "Networks": {
          "bridge": {
            "IPAMConfig": null,
            "Links": null,
            "Aliases": null,
            "NetworkID": "01890a0ca2af3305e04ab61e613911d891651bb16736f6251d0a2d47ea5cc351",
            "EndpointID": "",
            "Gateway": "",
            "IPAddress": "",
            "IPPrefixLen": 0,
            "IPv6Gateway": "",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "MacAddress": "",
            "DriverOpts": null
          }
        }
      },
      "Mounts": [
        {
          "Type": "volume",
          "Name": "earthly-cache",
          "Source": "/var/lib/docker/volumes/earthly-cache/_data",
          "Destination": "/tmp/earthly",
          "Driver": "local",
          "Mode": "z",
          "RW": true,
          "Propagation": ""
        },
        {
          "Type": "volume",
          "Name": "f33f1ff1b45acbbd20dc7e974f35cf8383034c594fe9a7dbbb9ec5356ad0999b",
          "Source": "",
          "Destination": "/var/lib/buildkit",
          "Driver": "local",
          "Mode": "",
          "RW": true,
          "Propagation": ""
        }
      ]
    }
  ]
}
```

### `containers/{id}`

Primitive used for the communication with the Docker API. Shows status of the container based on the requested id.

- Full route: `/api/{version}/containers/{id}`
- Method: **GET**
- Required content: *none*
- Response example:

```json
{
  "id": "54d",
  "status": "exited"
}
```

#### `containers/{id}/kill`

Primitive used for the communication with the Docker API. Kills container according on the requested id.

- Full route: `/api/{version}/containers/{id}/kill`
- Method: **GET**
- Required content: *none*
- Response example:

```json
{
  "id": "54d",
  "status": "exited"
}
```

#### `containers/{id}/start`

Primitive used for the communication with the Docker API. Starts container according to the requested id.

- Full route: `/api/{version}/containers/{id}/start`
- Method: **GET**
- Required content: *none*
- Response example:

```json
{
  "id": "54d",
  "status": "running"
}
```

#### `containers/{id}/stop`

Primitive used for the communication with the Docker API. Stops container according to the requested id.

- Full route: `/api/{version}/containers/{id}/stop`
- Method: **GET**
- Required content: *none*
- Response example:

```json
{
  "id": "54d",
  "status": "running"
}
```

## `services`

This endpoint is abstraction layer over containers primitive. By default it is used for displaying info about all service that can be accessed.

- Full route: `/api/{version}/services`
- Method: **POST**
- Required content: **empty**
- Content example: *none*
- Response example:

```json
{
  "services": [
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "dc0f94",
      "governor": false
    },
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "5ceabf",
      "governor": false
    }
  ]
}
```

### `services/{name}`

This endpoint is used for displaying info about a single requested service.

- Full route: `/api/{version}/services/{name}`
- Method: **POST**
- Required content: **empty**
- Content example: *none*
- Response example:

```json
{
  "services": [
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "dc0f94",
      "governor": false
    },
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "5ceabf",
      "governor": false
    }
  ]
}
```

#### `services/{name}/start`

This endpoint is used for starting the requested service.

- Full route: `/api/{version}/services/{name}/start`
- Method: **POST**
- Required content: **empty**
- Content example: *none*
- Response example:

```json
{
  "services": [
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "dc0f94",
      "governor": false
    },
    {
      "name": "nginx-2",
      "state": "running",
      "stack": "nginx",
      "id": "5ceabf",
      "governor": false
    }
  ]
}
```

#### `services/{name}/stop`

This endpoint is used for stopping the requested service.

- Full route: `/api/{version}/services/{name}/stop`
- Method: **POST**
- Required content: **empty**
- Content example: *none*
- Response example:

```json
{
  "services": [
    {
      "name": "nginx-2",
      "state": "exited",
      "stack": "nginx",
      "id": "dc0f94",
      "governor": false
    },
    {
      "name": "nginx-2",
      "state": "exited",
      "stack": "nginx",
      "id": "5ceabf",
      "governor": false
    }
  ]
}
```

## `storage`

<b label="todo" style="color:red">WARNING! Not implemented!</b>

This endpoint is used for retrieving data from the storage.

- Full route: `/api/{version}/storage`
- Method: **GET**
- Required content: **form**
- Content example:
- Response example: *TODO*

### `storage/{id}`

This endpoint is used for displaying info about a single requested service.

- Full route: `/api/{version}/storage/{id}`
- Method: **POST**
- Required content: **form**
- Content example:

```form
columns: {"col1":"varchar(255)","col2":"int"}
```

- Response example:

```json
```