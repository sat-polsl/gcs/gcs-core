package services

import "fmt"

var ErrServiceNotLabeled = fmt.Errorf("the service was not labeled correctly")

const (
	LabelGCSInclude    = "sat.gcs.include"        // bool
	LabelGCSService    = "sat.gcs.service.name"   // string
	LabelStack         = "sat.gcs.stack"          // string
	LabelStackGovernor = "sat.gcs.stack.governor" // bool
)
