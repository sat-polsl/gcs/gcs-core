// Package services holds API definition (for both Server and Client) for
// Services endpoint.
//
// Services are abstraction layer over Containers, allowing us to interact
// directly with services belonging only to Ground Control System, as opposed to
// the Containers, which allow us to interact with any container located on
// host.
package services

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/gorilla/mux"

	"gitlab.com/sat-polsl/gcs/gcs-core/containers"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

type API struct {
	Client *containers.Client
}

type Service struct {
	Name     string `json:"name"`
	State    string `json:"state"`
	Stack    string `json:"stack"`
	ID       string `json:"id"`
	Governor bool   `json:"governor"`
}

func NewService(c types.Container) (Service, error) {
	name := c.Labels[LabelGCSService]
	if len(name) == 0 {
		return Service{}, ErrServiceNotLabeled
	}

	gov, err := strconv.ParseBool(c.Labels[LabelStackGovernor])
	if err != nil {
		gov = false
	}

	return Service{
		Name:     name,
		Stack:    c.Labels[LabelStack],
		State:    c.State,
		ID:       fmt.Sprintf("%.6s", c.ID),
		Governor: gov,
	}, nil
}

func New() (*API, error) {
	c, err := containers.New(time.Second * 30)
	if err != nil {
		return nil, fmt.Errorf("unable to create Docker client %w", err)
	}

	return &API{
		Client: c,
	}, nil
}

func (a API) Close() error {
	if err := a.Client.Close(); err != nil {
		return fmt.Errorf("unable to close Docker client: %w", err)
	}

	return nil
}

func (a API) Register(mux *mux.Router) {
	tools.RegisterGet(mux, RouteBase, a.List)
	tools.RegisterGet(mux, RouteBase+RouteID, a.Status)

	containersMux := mux.PathPrefix(RouteBase + RouteID).Subrouter()
	tools.RegisterGet(containersMux, RouteStart, a.Start)
	tools.RegisterGet(containersMux, RouteStop, a.Stop)
}

type Filter struct {
	Name  string
	Value string
}

func filter(cont []types.Container, filter Filter) (rc []Service, err error) {
	for _, c := range cont {
		if c.Labels[filter.Name] == filter.Value {
			var s Service
			if s, err = NewService(c); err != nil {
				err = ErrServiceNotLabeled
			} else {
				rc = append(rc, s)
			}
		}
	}

	return
}

func (a API) Status(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	name := mux.Vars(r)["name"]

	c, err := a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	s, err := filter(c, Filter{Name: LabelGCSService, Value: name})
	if err != nil {
		tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
			"services": s,
			"errors":   err.Error(),
		})
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"services": s,
	})
}

func (a API) Start(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	name := mux.Vars(r)["name"]

	// list all containers
	c, err := a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	// filter raw containers
	fc, err := filter(c, Filter{Name: LabelGCSService, Value: name})
	if err != nil {
		tools.WriteError(w, r, http.StatusOK, err)
		return
	}

	// start each container from requested service
	var errs []error
	for _, cont := range fc {
		_, err := a.Client.StartContainer(ctx, cont.ID)
		if err != nil {
			errs = append(errs, err)
		}
	}

	// list once again to reload statuses
	c, err = a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	// filter, create list of services
	s, err := filter(c, Filter{Name: LabelGCSService, Value: name})
	if err != nil {
		tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
			"services": s,
			"errors":   append(errs, err),
		})
		return
	}

	if len(errs) != 0 {
		tools.WriteResponse(w, r, http.StatusUnauthorized, tools.ResponseMap{
			"services": s,
			"errors":   err.Error(),
		})
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"services": s,
	})
}

func (a API) Stop(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	name := mux.Vars(r)["name"]

	// list all containers
	c, err := a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	// filter raw containers
	fc, err := filter(c, Filter{Name: LabelGCSService, Value: name})
	if err != nil {
		tools.WriteError(w, r, http.StatusOK, err)
		return
	}

	// start each container from requested service
	var errs []error
	for _, cont := range fc {
		_, err := a.Client.StopContainer(ctx, cont.ID)
		if err != nil {
			errs = append(errs, err)
		}
	}

	// list once again to reload statuses
	c, err = a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	// filter, create list of services
	s, err := filter(c, Filter{Name: LabelGCSService, Value: name})
	if err != nil {
		tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
			"services": s,
			"errors":   append(errs, err),
		})
		return
	}

	if len(errs) != 0 {
		tools.WriteResponse(w, r, http.StatusUnauthorized, tools.ResponseMap{
			"services": s,
			"errors":   err.Error(),
		})
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"services": s,
	})
}

func (a API) List(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	c, err := a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	s, err := filter(c, Filter{Name: LabelGCSInclude, Value: "true"})
	if err != nil {
		tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
			"services": s,
			"errors":   err.Error(),
		})
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"services": s,
	})
}
