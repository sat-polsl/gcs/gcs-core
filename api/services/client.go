package services

import (
	"fmt"
	"net/http"
	"path"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/common"
)

type ClientOpts struct {
	Protocol      string
	Client        *http.Client
	RouteRoot     string
	ServerAddress string
	ServerPort    string
}

func (opts *ClientOpts) Parse() ClientOpts {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return *opts
}

type Services struct {
	client    *http.Client
	routeRoot string
	target    string
}

func NewClient(opts ClientOpts) Services {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return Services{
		client:    opts.Client,
		routeRoot: opts.RouteRoot,
		target:    fmt.Sprintf("%s://%s%s", opts.Protocol, opts.ServerAddress, opts.ServerPort),
	}
}

func (s Services) List() (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Services) Status(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase, id)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Services) Start(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase, id, RouteStart)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Services) Stop(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase, id, RouteStop)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}
