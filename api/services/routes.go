package services

const (
	RouteBase  = "/services"
	RouteID    = "/{name}" // extends RouteBase
	RouteStart = "/start"  // extends RouteBase/RouteId
	RouteStop  = "/stop"   // extends RouteBase/RouteId
)
