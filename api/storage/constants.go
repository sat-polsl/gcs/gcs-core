package storage

import "time"

const (
	OK          = "OK"
	ParamFormat = "format"
	FormatJSON  = "JSON"
	FormatCSV   = "CSV"
)

const (
	DefaultTimeout = 10 * time.Second
)
