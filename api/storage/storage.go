// Package storage holds API definition (for both Server and Client) for Storage
// endpoint.
package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

type API struct {
	registry storage.MissionRegistry
}

func New() *API {
	registry := storage.BackendRegistryFactory{}.New()

	return &API{
		registry: registry,
	}
}

func (a API) Close() error {
	if err := a.registry.Close(context.Background()); err != nil {
		return fmt.Errorf("unable to close Docker client: %w", err)
	}

	return nil
}

func (a API) Register(mux *mux.Router) {
	tools.RegisterGet(mux, RouteBase, a.List)
	tools.RegisterGet(mux, RouteBase+RouteID, a.Get)
	tools.RegisterGet(mux, RouteBase+RouteID+RouteInfo, a.Info)

	tools.RegisterPost(mux, RouteBase+RouteID, a.Create)
	tools.RegisterPatch(mux, RouteBase+RouteID, a.Update)
}

func (a API) List(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	defer cancel()

	c, err := a.registry.ListMissions(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"missions": c,
	})
}

func (a API) Info(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]

	info, err := a.registry.MissionInfo(ctx, id)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":   id,
		"info": info,
	})
}

func (a API) Get(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]
	format := r.URL.Query()["format"]

	values, err := a.registry.GetValues(ctx, id)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	if len(format) > 0 {
		switch format[0] {
		case "csv", "CSV":
			tools.WriteCSV(w, r, http.StatusOK, toCSV(values))
		case "json", "JSON":
			tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
				"id":     id,
				"values": values,
			})
		default:
			tools.WriteError(w, r, http.StatusBadRequest, fmt.Errorf("wrong format specifier: %s", format[0]))
		}
	} else {
		tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
			"id":     id,
			"values": values,
		})
	}
}

func toCSV(values []map[string]interface{}) string {
	var rows []string
	for _, r := range values {
		var row []string
		for _, v := range r {
			switch v.(type) {
			case string:
				row = append(row, v.(string))
			default:
				row = append(row, fmt.Sprintf("%#v", v))
			}
		}
		rows = append(rows, strings.Join(row, ","))
	}

	return strings.ReplaceAll(strings.Join(rows, "\n"), "<nil>", "")
}

func (a API) Create(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]
	description := r.FormValue("description")
	columnsJSON := r.FormValue("columns")

	columns := make(map[string]string)
	if err := json.Unmarshal([]byte(columnsJSON), &columns); err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, fmt.Errorf("unable to unmarshall columns: %w", err))
		return
	}

	if err := a.registry.CreateMission(ctx, id, description, columns); err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": OK,
	})
}

func (a API) Update(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]
	description := r.FormValue("description")

	if err := a.registry.UpdateMission(ctx, id, description); err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": OK,
	})
}
