package storage

import (
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/common"
)

type ClientOpts struct {
	Protocol      string
	Client        *http.Client
	RouteRoot     string
	ServerAddress string
	ServerPort    string
}

func (opts *ClientOpts) Parse() ClientOpts {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return *opts
}

type Storage struct {
	client    *http.Client
	routeRoot string
	target    string
}

func NewClient(opts ClientOpts) Storage {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return Storage{
		client:    opts.Client,
		routeRoot: opts.RouteRoot,
		target:    fmt.Sprintf("%s://%s%s", opts.Protocol, opts.ServerAddress, opts.ServerPort),
	}
}

func (s Storage) List() (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Storage) Info(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase, id, RouteInfo)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Storage) Create(id string, description string, frameContent string) (r common.Response, err error) {
	form := url.Values{}
	form.Add("columns", frameContent)
	form.Add("description", description)

	method := http.MethodPost
	route := s.target + path.Join(s.routeRoot, RouteBase, id)

	req, err := common.CreateRequest(method, route, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Storage) Update(id string, description string) (r common.Response, err error) {
	form := url.Values{}
	form.Add("description", description)

	method := http.MethodPatch
	route := s.target + path.Join(s.routeRoot, RouteBase, id)

	req, err := common.CreateRequest(method, route, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}

	r, err = common.ParseResponse(s.client.Do(req))

	return
}

func (s Storage) GetValues(id string, csv bool) (r common.Response, err error) {
	form := url.Values{}

	method := http.MethodGet
	route := s.target + path.Join(s.routeRoot, RouteBase, id)

	req, err := common.CreateRequest(method, route, strings.NewReader(form.Encode()))
	if err != nil {
		return
	}

	if !csv {
		q := req.URL.Query()
		q.Add(ParamFormat, FormatJSON)
		req.URL.RawQuery = q.Encode()
		r, err = common.ParseResponse(s.client.Do(req))
	} else {
		q := req.URL.Query()
		q.Add(ParamFormat, FormatCSV)
		req.URL.RawQuery = q.Encode()
		r, err = common.ParseCSVResponse(s.client.Do(req))
	}

	return
}
