// Package containers holds API definition (for both Server and Client) for
// Containers endpoint.
//
//	It allows us to interact with any container located on host through the REST API.
package containers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/sat-polsl/gcs/gcs-core/containers"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

type API struct {
	insecure bool
	Client   *containers.Client
}

func New() (*API, error) {
	c, err := containers.New(time.Second * 30)
	if err != nil {
		return nil, fmt.Errorf("unable to create Docker client %w", err)
	}

	return &API{
		Client: c,
	}, nil
}

func (a API) Close() error {
	if err := a.Client.Close(); err != nil {
		return fmt.Errorf("unable to close Docker client: %w", err)
	}

	return nil
}

func (a API) Register(mux *mux.Router) {
	tools.RegisterGet(mux, RouteBase, a.List)
	tools.RegisterGet(mux, RouteBase+RouteID, a.Status)

	containersMux := mux.PathPrefix(RouteBase + RouteID).Subrouter()
	tools.RegisterGet(containersMux, RouteKill, a.Kill)
	tools.RegisterGet(containersMux, RouteStart, a.Start)
	tools.RegisterGet(containersMux, RouteStop, a.Stop)
}

func (a API) List(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	c, err := a.Client.ListContainers(ctx)
	if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"containers": c,
	})
}

func (a API) Status(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]

	status, err := a.Client.ContainerStatus(ctx, id)
	if err != nil && tools.ErrContains(err, "No such container") {
		tools.WriteError(w, r, http.StatusNotFound, fmt.Errorf("container id:%s not found", id))
		return
	} else if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": status,
	})
}

func (a API) Kill(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]

	status, err := a.Client.KillContainer(ctx, id)
	if err != nil && tools.ErrContains(err, "No such container") {
		tools.WriteError(w, r, http.StatusNotFound, fmt.Errorf("container id:%s not found", id))
		return
	} else if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": status,
	})
}

func (a API) Start(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]
	status, err := a.Client.StartContainer(ctx, id)
	if err != nil && tools.ErrContains(err, "No such container") {
		tools.WriteError(w, r, http.StatusNotFound, fmt.Errorf("container id:%s not found", id))
		return
	} else if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": status,
	})
}

func (a API) Stop(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	defer cancel()

	id := mux.Vars(r)["id"]

	status, err := a.Client.StopContainer(ctx, id)
	if err != nil && tools.ErrContains(err, "No such container") {
		tools.WriteError(w, r, http.StatusNotFound, fmt.Errorf("container id:%s not found", id))
		return
	} else if err != nil {
		tools.WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	tools.WriteResponse(w, r, http.StatusOK, tools.ResponseMap{
		"id":     id,
		"status": status,
	})
}
