package containers

import "github.com/docker/docker/api/types"

type ErrorResponse struct {
	Error string `json:"error"`
}

type ListResponse struct {
	Containers []types.Container `json:"containers"`
}

type StatusResponse struct {
	ID     string `json:"id"`
	Status string `json:"status"`
}
