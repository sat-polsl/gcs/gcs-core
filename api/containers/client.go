package containers

import (
	"fmt"
	"net/http"
	"path"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/common"
)

type ClientOpts struct {
	Protocol      string
	Client        *http.Client
	RouteRoot     string
	ServerAddress string
	ServerPort    string
}

func (opts *ClientOpts) Parse() ClientOpts {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return *opts
}

type Containers struct {
	client    *http.Client
	routeRoot string
	target    string
}

func NewClient(opts ClientOpts) Containers {
	if opts.Protocol == "" {
		opts.Protocol = "http"
	}

	if opts.ServerPort == "" {
		opts.ServerPort = ":8080"
	}

	return Containers{
		client:    opts.Client,
		routeRoot: opts.RouteRoot,
		target:    fmt.Sprintf("%s://%s%s", opts.Protocol, opts.ServerAddress, opts.ServerPort),
	}
}

func (c Containers) List() (r common.Response, err error) {
	method := http.MethodGet
	route := c.target + path.Join(c.routeRoot, RouteBase)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(c.client.Do(req))

	return
}

func (c Containers) Status(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := c.target + path.Join(c.routeRoot, RouteBase, id)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(c.client.Do(req))

	return
}

func (c Containers) Start(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := c.target + path.Join(c.routeRoot, RouteBase, id, RouteStart)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(c.client.Do(req))

	return
}

func (c Containers) Stop(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := c.target + path.Join(c.routeRoot, RouteBase, id, RouteStop)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(c.client.Do(req))

	return
}

func (c Containers) Kill(id string) (r common.Response, err error) {
	method := http.MethodGet
	route := c.target + path.Join(c.routeRoot, RouteBase, id, RouteKill)

	req, err := common.CreateRequest(method, route, nil)
	if err != nil {
		return
	}

	r, err = common.ParseResponse(c.client.Do(req))

	return
}
