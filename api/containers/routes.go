package containers

// TODO
const (
	RouteBase  = "/containers"
	RouteID    = "/{id}"  // extends RouteBase
	RouteKill  = "/kill"  // extends RouteBase/RouteId
	RouteStart = "/start" // extends RouteBase/RouteId
	RouteStop  = "/stop"  // extends RouteBase/RouteId
)
