// Package api contains definitions of all REST API endpoints used in the Ground
// Control System.
package api

import (
	"fmt"
	"net/http"
	"path"

	// used for providing profiling endpoint
	_ "net/http/pprof"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/sat-polsl/gcs/gcs-core/api/auth"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/containers"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/services"
	"gitlab.com/sat-polsl/gcs/gcs-core/api/storage"
	auth_tool "gitlab.com/sat-polsl/gcs/gcs-core/auth"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type API struct {
	Containers *containers.API
	Services   *services.API
	Auth       *auth.API
	Storage    *storage.API
	insecure   bool
}

func New(insecure bool) (*API, error) {
	c, err := containers.New()
	if err != nil {
		return nil, fmt.Errorf("unable to create Docker client: %w", err)
	}

	s, err := services.New()
	if err != nil {
		return nil, fmt.Errorf("unable to create Docker client: %w", err)
	}

	a, err := auth.New()
	if err != nil {
		return nil, fmt.Errorf("unable to auth API: %w", err)
	}

	b := storage.New()
	if err != nil {
		return nil, fmt.Errorf("unable to auth API: %w", err)
	}

	return &API{
		Containers: c,
		Services:   s,
		Auth:       a,
		Storage:    b,

		insecure: insecure,
	}, nil
}

func (a API) Close() {
	if err := a.Containers.Close(); err != nil {
		l.Log().WithFields(logrus.Fields{
			l.From: "api",
			l.At:   "containers",
		}).Warnf("unable to close containers API endpoint: %s", err.Error())
	}
	if err := a.Services.Close(); err != nil {
		l.Log().WithFields(logrus.Fields{
			l.From: "api",
			l.At:   "services",
		}).Warnf("unable to close services API endpoint: %s", err.Error())
	}
	if err := a.Auth.Close(); err != nil {
		l.Log().WithFields(logrus.Fields{
			l.From: "api",
			l.At:   "auth",
		}).Warnf("unable to close auth API endpoint: %s", err.Error())
	}
}

func (a API) Register(mux *mux.Router) {
	mux.PathPrefix("/debug/pprof/").Handler(http.DefaultServeMux)
	mux.Use(a.commonMiddleware)

	api := mux.PathPrefix(path.Join(RouteBase, RouteVersion)).Subrouter()

	a.Containers.Register(api)
	a.Services.Register(api)
	a.Auth.Register(api)
	a.Storage.Register(api)
}

func (a API) commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		l.Log().WithFields(logrus.Fields{
			l.At:        r.URL.Path,
			Method:      r.Method,
			Agent:       r.UserAgent(),
			QueryParams: r.URL.Query(),
		}).Info("request received")

		if !a.insecure {
			if err := auth_tool.ValidateAPIKey(r); err != nil {
				tools.WriteError(w, r, http.StatusUnauthorized, err)
				return
			}
		}

		w.Header().Add("Content-Type", "application/json")

		next.ServeHTTP(w, r)
	})
}
