package containers

import "time"

// Default values for Docker host endpoint
const (
	DefaultTimeout = 45 * time.Second
)

// Used inside the DockerCtlMessage struct
const (
	UNKNOWN = iota
	START   = iota
	STOP    = iota
	KILL    = iota
)
