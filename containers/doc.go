// Package containers is a wrapper around the official Docker API.
//
// See also:
//
// * https://docs.docker.com/engine/api/sdk
package containers
