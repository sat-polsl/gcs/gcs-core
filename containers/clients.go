package containers

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

type Client struct {
	dockerClient *client.Client
	timeout      time.Duration
}

// New creates new client according to endpoint url from ENV with default
// hostconfig
func New(timeout time.Duration) (*Client, error) {
	client, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("unable to create client: %w", err)
	}

	return &Client{dockerClient: client}, nil
}

func (c Client) Close() error {
	return c.dockerClient.Close()
}

func (c Client) GetRawClient() *client.Client {
	return c.dockerClient
}

func (c Client) PrepareMockContainer(ctx context.Context) (id string, err error) {
	image := "alpine"

	reader, err := c.dockerClient.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		err = fmt.Errorf("unable to pull mock container image: %w", err)

		return
	}
	io.Copy(io.Discard, reader)

	resp, err := c.dockerClient.ContainerCreate(ctx, &container.Config{
		Image: image,
		Tty:   false,
		Cmd:   []string{"sleep", fmt.Sprint(60 * 60 * 24)},
	}, nil, nil, nil, "")
	if err != nil {
		err = fmt.Errorf("unable to create mock container: %w", err)

		return
	}

	if err = c.dockerClient.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		err = fmt.Errorf("unable to start mock container: %w", err)

		return
	}

	return resp.ID, nil
}

func (c Client) RemoveMockContainer(ctx context.Context, containerID string) (err error) {
	if err = c.dockerClient.ContainerRemove(ctx, containerID, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		Force:         true,
	}); err != nil {
		err = fmt.Errorf("unable to remove mock container: %w", err)

		return
	}

	return
}

// ContainerStatus gets information about container's status, Can be one of
// "created", "running", "paused", "restarting", "removing", "exited", or "dead"
func (c Client) ContainerStatus(ctx context.Context, containerID string) (status string, err error) {
	cont, err := c.dockerClient.ContainerInspect(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("error on inspect: %w", err)

		return
	}

	return cont.State.Status, nil
}

// StopContainer stops container specified by the containerID string
func (c Client) StopContainer(ctx context.Context, containerID string) (status string, err error) {
	err = c.dockerClient.ContainerStop(ctx, containerID, &c.timeout)
	if err != nil {
		err = fmt.Errorf("error on stop request: %w", err)

		return
	}

	status, err = c.ContainerStatus(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("error getting container status: %w", err)

		return
	}

	return
}

// StartContainer starts container with containerID.
func (c Client) StartContainer(ctx context.Context, containerID string) (status string, err error) {
	err = c.dockerClient.ContainerStart(ctx, containerID, types.ContainerStartOptions{})
	if err != nil {
		err = fmt.Errorf("error on start request: %w", err)

		return
	}

	status, err = c.ContainerStatus(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("error getting container status: %w", err)

		return
	}

	return
}

// KillContainer kills container with containerID.
func (c Client) KillContainer(ctx context.Context, containerID string) (status string, err error) {
	err = c.dockerClient.ContainerKill(ctx, containerID, "SIGKILL")
	if err != nil {
		err = fmt.Errorf("error on kill request: %w", err)

		return
	}

	status, err = c.ContainerStatus(ctx, containerID)
	if err != nil {
		err = fmt.Errorf("error getting container status: %w", err)

		return
	}

	return
}

// ListContainers lists all running and stopped containers.
func (c Client) ListContainers(ctx context.Context) (conts []types.Container, err error) {
	conts, err = c.dockerClient.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		err = fmt.Errorf("error on list containers request: %w", err)
		return
	}

	return
}
