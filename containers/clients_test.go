package containers_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/sat-polsl/gcs/gcs-core/containers"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

func TestNewClient_Smoke(t *testing.T) {
	tools.SkipTestOnCI(t)

	_, err := containers.New(30 * time.Second)
	if err != nil {
		t.Errorf("error creating client: %v", err)
	}
}

func TestClientMethods(t *testing.T) {
	tools.SkipTestOnCI(t)

	var (
		err         error
		containerID string
		status      string
		c           *containers.Client
	)

	if c, err = containers.New(10 * time.Second); err != nil {
		t.Errorf("error during creating client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if containerID, err = c.PrepareMockContainer(ctx); err != nil {
		t.Errorf("error during preparing test: %v", err)
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if status, err = c.StopContainer(ctx, containerID); err != nil {
		t.Errorf("error during stoping container: %v", err)
	} else if status != "exited" {
		t.Errorf("stoping container: got: %v wanted :%v", status, "exited")
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if status, err = c.StartContainer(ctx, containerID); err != nil {
		t.Errorf("error during starting container: %v", err)
	} else if status != "running" {
		t.Errorf("starting container: got: %v wanted :%v", status, "running")
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if status, err = c.KillContainer(ctx, containerID); err != nil {
		t.Errorf("error during killing container: %v", err)
	} else if status != "exited" {
		t.Errorf("killing container: got: %v wanted :%v", status, "exited")
	}
	cancel()

	ctx, cancel = context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if err = c.RemoveMockContainer(ctx, containerID); err != nil {
		t.Errorf("error during performing cleanup %v", err)
	}
	cancel()
}

func TestListContainers(t *testing.T) {
	tools.SkipTestOnCI(t)

	var (
		err error
		c   *containers.Client
	)

	if c, err = containers.New(10 * time.Second); err != nil {
		t.Errorf("error during creating client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), containers.DefaultTimeout)
	if _, err = c.ListContainers(ctx); err != nil {
		t.Errorf("error during getting list of container: %v", err)
	}
	cancel()
}
