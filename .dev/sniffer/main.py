import zmq
import os

def main():
    print(f'zmq version: {zmq.zmq_version()}')

    host = os.getenv('PROXY_IP')
    if not host:
        raise Exception('no proxy ip')
    port = os.getenv('SUB_PORT')
    if not port:
        raise Exception('no proxy port')

    address = f'tcp://{host}:{port}'
    print(f'address: {address}')

    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    # We must declare the socket as of type SUBSCRIBER, and pass a prefix filter.
    # Here, the filter is the empty string, wich means we receive all messages.
    # We may subscribe to several filters, thus receiving from all.
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    # We can connect to several endpoints if we desire, and receive from all.
    socket.connect(address)

    print('started sniffer')
    while True:
        print(socket.recv())

if __name__ == '__main__':
    main()
