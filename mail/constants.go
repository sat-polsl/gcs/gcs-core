package mail

import "fmt"

var ErrNoRecipient = fmt.Errorf("no recipient provided")

const (
	EnvSMTPUser     = "SMTP_USER"
	EnvSMTPPassword = "SMTP_PASSWORD"
	EnvSMTPEmail    = "SMTP_EMAIL"
	EnvSMTPHost     = "SMTP_HOST"
	EnvSMTPPort     = "SMTP_PORT"
	EnvSMTPSecure   = "SMTP_SECURE"

	EnvAllowedDomains = "ALLOWED_DOMAINS"
)

const MailBody = `
Welcome to Ground Control System!

Here is your API key: %s

Keep it in a safe place!

If you want to revoke your API key, send remove request with your API key.

If you want to remove all your API keys, send prune request with your API key.

May the force be with you!
`
