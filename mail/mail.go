// Package mail provides basic email sending functionality.
//
// It's primary purpose is to send email messages with API keys to the users who
// request them, using SMTP.
package mail

import (
	"crypto/tls"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	gomail "gopkg.in/mail.v2"

	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type Client struct {
	allowedDomains []string
	Email          string
	user           string
	password       string
	port           int
	smtpHost       string
	secure         bool
}

func NewClient() Client {
	host := os.Getenv(EnvSMTPHost)
	if len(host) < 1 {
		host = "smtp.gmail.com"
		l.Log().WithFields(logrus.Fields{
			l.From:      "smtp-new-client",
			"smtp-host": host,
		}).Warnln("SMTP host not set, falling back to default")
	}

	port, err := strconv.Atoi(os.Getenv(EnvSMTPPort))
	if err != nil {
		port = 587

		l.Log().WithFields(logrus.Fields{
			l.From:      "smtp-new-client",
			"smtp-port": port,
		}).Warnln("SMTP port not set, falling back to default")
	}

	d := os.Getenv(EnvAllowedDomains)
	if len(d) < 1 {
		d = "polsl.pl"

		l.Log().WithFields(logrus.Fields{
			l.From:            "smtp-new-client",
			"allowed-domains": d,
		}).Warnln("list of allowed domains not set, falling back to default")
	}

	secure, err := strconv.ParseBool(os.Getenv(EnvSMTPSecure))
	if err != nil {
		secure = true

		l.Log().WithFields(logrus.Fields{
			l.From:        "smtp-new-client",
			"smtp-secure": secure,
		}).Warnln("no information about using SSL/TLS in SMTP, falling back to default")
	}

	domains := strings.Split(d, ",")

	user := os.Getenv(EnvSMTPUser)
	if len(user) < 1 {
		user = os.Getenv(EnvSMTPEmail)

		l.Log().WithFields(logrus.Fields{
			l.From:      "smtp-new-client",
			"smtp-user": user,
		}).Warnln("username not set, falling back to default (SMTP_EMAIL)")
	}

	return Client{
		Email:          os.Getenv(EnvSMTPEmail),
		user:           user,
		password:       os.Getenv(EnvSMTPPassword),
		secure:         secure,
		port:           port,
		smtpHost:       host,
		allowedDomains: domains,
	}
}

// SendAPIKey sends API key with default message to email.
func (c Client) SendAPIKey(email, apiKey string) error {
	return c.SendMail([]string{email}, "GCS API key", fmt.Sprintf(MailBody, apiKey))
}

// SendMail sends mail with subject and body to emails.
func (c Client) SendMail(emails []string, subject, body string) error {
	m := gomail.NewMessage()

	m.SetHeader("From", c.Email)
	if len(emails) > 1 {
		m.SetHeader("To", emails[0])

		var cc []string
		for _, t := range emails[1:] {
			cc = append(cc, fmt.Sprintf(`"%s"`, t))
		}

		m.SetHeader("Cc", strings.Join(cc, ", "))
	} else if len(emails) == 1 {
		m.SetHeader("To", emails[0])
	} else {
		return ErrNoRecipient
	}
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", body)

	d := gomail.NewDialer(c.smtpHost, c.port, c.user, c.password)
	if c.secure {
		d.TLSConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	} else {
		d.SSL = false
	}

	if err := d.DialAndSend(m); err != nil {
		return fmt.Errorf("unable to send email: %w", err)
	}

	return nil
}

// DomainOk returns true if mail is in the allowed domains list.
func (c Client) DomainOk(mail string) bool {
	for _, d := range c.allowedDomains {
		reg := regexp.MustCompile(".+" + d)
		if reg.MatchString(mail) {
			return true
		}
	}

	return false
}
