package mail_test

import (
	"os"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
	"gitlab.com/sat-polsl/gcs/gcs-core/mail"
)

func TestMain(m *testing.M) {
	if err := tools.SetEnvs(tools.EnvVar{
		mail.EnvAllowedDomains: os.Getenv("TEST_ALLOWED_DOMAINS"),
		mail.EnvSMTPEmail:      os.Getenv("TEST_EMAIL_ADDRESS"),
		mail.EnvSMTPPassword:   os.Getenv("TEST_SMTP_PASSWORD"),
		mail.EnvSMTPUser:       os.Getenv("TEST_SMTP_USER"),
		mail.EnvSMTPHost:       os.Getenv("TEST_SMTP_HOST"),
		mail.EnvSMTPPort:       os.Getenv("TEST_SMTP_PORT"),
		mail.EnvSMTPSecure:     os.Getenv("TEST_SMTP_SECURE"),
	}); err != nil {
		os.Exit(1)
	}

	// run tests
	code := m.Run()

	os.Exit(code)
}

func TestMail(t *testing.T) {
	tools.SkipTestOnCI(t)

	to := os.Getenv("TEST_EMAIL_ADDRESS")

	c := mail.NewClient()

	if !c.DomainOk(to) {
		t.Errorf("domain not in the list")
	}

	if err := c.SendMail([]string{to}, "TEST-MESSAGE", "Test Body"); err != nil {
		t.Errorf("unable to send API key: %s", err.Error())
	}

	if err := c.SendAPIKey(to, "TEST-KEY"); err != nil {
		t.Errorf("unable to send API key: %s", err.Error())
	}
}
