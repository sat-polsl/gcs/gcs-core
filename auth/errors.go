package auth

import "fmt"

var (
	ErrInvalidAPIKey = fmt.Errorf("provided key is invalid")
	ErrNoAPIKey      = fmt.Errorf("no API key was provided")
	ErrTokenFormat   = fmt.Errorf("bearer token was not formatted properly")
)
