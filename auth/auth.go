// Package auth provides functions to verify and validate Bearer Tokens.
package auth

import (
	"net/http"
	"regexp"

	"github.com/sirupsen/logrus"

	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

// ValidateAPIKey validates the Authentication field in r.
func ValidateAPIKey(r *http.Request) error {
	token, err := getToken(r)
	if err != nil {
		return err
	}

	l.Log().WithFields(logrus.Fields{
		"token": l.Anonimize(token, 3, 13),
	}).Trace("API Key validation")

	// TODO: verify API

	// TODO: return ErrInvalidAPIKey

	return nil
}

func getToken(r *http.Request) (string, error) {
	token := getTokenHeader(r)

	if len(token) == 0 {
		token = getTokenQuery(r)
	}

	if len(token) == 0 {
		return token, ErrNoAPIKey
	}

	return token, nil
}

func getTokenQuery(r *http.Request) string {
	var token string

	if err := r.ParseForm(); err != nil {
		l.Log().WithField(l.At, r.URL).Errorf("error parsing form: %s", err.Error())
	}

	re := regexp.MustCompile("/debug/pprof/.*?.*X-GCSCore-Token=.+")

	if re.MatchString(r.URL.String()) {
		token = r.Form["X-GCSCore-Token"][0]
	}

	return token
}

func getTokenHeader(r *http.Request) string {
	return r.Header.Get(TokenHeader)
}
