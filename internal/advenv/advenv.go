// Package advenv ("advanced environmend") implements a parser for complex
// environment variables.
package advenv

import (
	"os"
	"reflect"
	"strconv"
	"strings"
)

const is64Bit = uint64(^uintptr(0)) == ^uint64(0)

type ParsedEnv struct {
	V interface{}
	T reflect.Kind
}

type AdvancedEnvParser struct {
	separator string
}

func NewAdvancedEnvParser(separator string) AdvancedEnvParser {
	return AdvancedEnvParser{separator: separator}
}

var s AdvancedEnvParser = NewAdvancedEnvParser(":")

func (s AdvancedEnvParser) Parse(name string, expected ...string) map[string]ParsedEnv {
	env := os.Getenv(name)

	out := make(map[string]ParsedEnv)

	kvPairs := strings.Split(env, s.separator)

	for _, kvPair := range kvPairs {
		kv := strings.SplitN(kvPair, "=", 2)
		if len(kv) == 2 {
			out[kv[0]] = s.convert(kv[1])
		}
	}

	return out
}

func (s AdvancedEnvParser) convert(data string) ParsedEnv {
	var bitSize int
	if is64Bit {
		bitSize = 64
	} else {
		bitSize = 32
	}

	i, err := strconv.Atoi(data)
	if err == nil {
		return ParsedEnv{
			V: i,
			T: reflect.Int,
		}
	}

	f, err := strconv.ParseFloat(data, bitSize)
	if err == nil {
		if bitSize == 64 {
			return ParsedEnv{
				V: f,
				T: reflect.Float64,
			}
		}

		if bitSize == 32 {
			return ParsedEnv{
				V: f,
				T: reflect.Float32,
			}
		}
	}

	b, err := strconv.ParseBool(data)
	if err == nil {
		return ParsedEnv{
			V: b,
			T: reflect.Bool,
		}
	}

	return ParsedEnv{
		V: data,
		T: reflect.String,
	}
}

func Parse(name string, expected ...string) (out map[string]ParsedEnv) {
	return s.Parse(name, expected...)
}
