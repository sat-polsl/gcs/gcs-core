package advenv_test

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/advenv"
)

var (
	EnvAdvenv        = "ADVENV_TEST"
	EnvAdvenvContent = `param_bool=true:param_str="string":param_int=1:param_float=1.23`
)

func TestMain(m *testing.M) {
	os.Setenv(EnvAdvenv, EnvAdvenvContent)

	// run tests
	code := m.Run()

	os.Exit(code)
}

func TestParse(t *testing.T) {
	parsed := advenv.Parse(EnvAdvenv)

	if len(parsed) < 1 {
		t.Errorf("wrong unmarshalling, got %d, wanted %d", len(parsed), len(strings.Split(EnvAdvenvContent, ":")))
	}

	for k, v := range parsed {
		switch k {
		case "param_bool":
			if v.T != reflect.Bool {
				t.Errorf("wrong type of parsed %s: got %s, wanted %s", k, v.T.String(), reflect.Bool.String())
			}

		case "param_str":
			if v.T != reflect.String {
				t.Errorf("wrong type of parsed %s: got %s, wanted %s", k, v.T.String(), reflect.String.String())

			}

		case "param_int":
			if v.T != reflect.Int {
				t.Errorf("wrong type of parsed %s: got %s, wanted %s", k, v.T.String(), reflect.Int.String())

			}

		case "param_float":
			if v.T != reflect.Float32 && v.T != reflect.Float64 {
				t.Errorf("wrong type of parsed %s: got %s, wanted %s or %s",
					k,
					v.T.String(),
					reflect.Float32.String(),
					reflect.Float64.String())
			}
		}
	}

	x := parsed["expected_not_in_env"].V
	if x != nil {
		t.Errorf("unexpected behavior, got %v, wanted %v", x, nil)
	}

	if x == nil || x.(bool) {
		t.Log("ok")
	} else {
		t.Errorf("unexpected behavior")
	}
}
