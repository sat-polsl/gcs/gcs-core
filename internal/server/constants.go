package server

// Constants containing names of ENV variables
const (
	EnvAPIPort  = "API_PORT"
	EnvInsecure = "INSECURE"
)

// TODO
const (
	DefaultAddress = ":8080"
)

// TODO
const (
	Port        = "port"
	FullAddress = "address"
)
