package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/sat-polsl/gcs/gcs-core/api"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type WebServer struct {
	Server *http.Server
}

func New() (*WebServer, error) {
	router := mux.NewRouter()

	insecure, _ := strconv.ParseBool(os.Getenv(EnvInsecure))

	a, err := api.New(insecure)
	if err != nil {
		return nil, fmt.Errorf("unable to create API: %w", err)
	}

	a.Register(router)

	addr := os.Getenv(EnvAPIPort)
	if len(addr) == 0 {
		l.Log().WithField(l.From, "server").Warnf("API port not set, falling back to default: %s", DefaultAddress)
		addr = DefaultAddress
	}

	return &WebServer{
		Server: &http.Server{
			Addr:    addr,
			Handler: router,
		},
	}, nil
}

func (w WebServer) String() string {
	return "api server"
}

func (w WebServer) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	go w.run()

	for {
		select {
		case <-ctx.Done():
			l.Log().WithField(l.From, "docker").Info("cancelled, closing")

			cctx, close := context.WithTimeout(context.Background(), time.Second*5)
			defer close()

			err := w.Server.Shutdown(cctx)
			if err != nil {
				l.Log().WithField(l.From, "docker").Errorf("shutting down server failed: %s", err.Error())
			}
			return nil
		}
	}
}

func (w WebServer) run() {
	l.Log().WithFields(logrus.Fields{
		l.From:      "docker-listen-and-serve",
		Port:        w.Server.Addr,
		FullAddress: fmt.Sprintf("http://localhost%s", w.Server.Addr),
	}).Infof("API server started")

	if err := w.Server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		l.Log().WithField(l.From, "docker-listen-and-serve").Errorf("error during Listen and Serve: %s", err.Error())
	}
}
