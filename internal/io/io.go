// Package io contains implementation of Publisher and Subscriber structs that
// are intended to run as a I/O threads.
package io

import "gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"

const (
	DefaultPubPort       = "14789"
	DefaultSubPort       = "14790"
	DefaultProxyEndpoint = "localhost"
)

var DefaultTopics = []string{
	threads.SubRotorChannel,
	threads.CriticalNotificationChanel,
}

type Frame struct {
	Topic   string
	Content string
}
