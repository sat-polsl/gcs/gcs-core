package io

// Constants containing names of ENV variables
const (
	EnvProxyEndpoint  = "PROXY_ENDPOINT"
	EnvPublisherPort  = "PUB_PORT"
	EnvSubscriberPort = "SUB_PORT"
)
