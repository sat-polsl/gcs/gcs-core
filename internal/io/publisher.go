package io

import (
	"context"
	"fmt"
	"os"
	"sync"

	"github.com/go-zeromq/zmq4"
	"github.com/sirupsen/logrus"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

// Publisher
type Publisher struct {
	ProxyEndpoint string
	Port          string
}

// NewPublisher
func NewPublisher() Publisher {
	proxyEndpoint := os.Getenv(EnvProxyEndpoint)
	if len(proxyEndpoint) < 1 {
		proxyEndpoint = DefaultProxyEndpoint
		l.Log().WithFields(logrus.Fields{
			l.From:           "publisher",
			"proxy_endpoint": proxyEndpoint,
		}).Warn("default proxy endpoint not set, falling back to default")
	}

	port := os.Getenv(EnvPublisherPort)
	if len(port) < 1 {
		port = DefaultPubPort
		l.Log().WithFields(logrus.Fields{
			l.From:     "publisher",
			"pub_port": port,
		}).Warn("default pub port not set, falling back to default")
	}

	return Publisher{
		ProxyEndpoint: proxyEndpoint,
		Port:          port,
	}
}

func (p Publisher) String() string {
	return "publisher"
}

// Start
func (p Publisher) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	l.Log().WithField(l.From, "publisher").Trace("starting")

	pub, err := p.dialPub(ctx)
	if err != nil {
		return err
	}
	defer (*pub).Close()

	ipcRecvChan, err := ipc.Get(threads.PubRotorChanel)
	if err != nil {
		return fmt.Errorf("unable to get storage channel: %w", err)
	}

	l.Log().WithField(l.From, "publisher").Trace("up and running")

	for {
		select {
		case tMsg := <-ipcRecvChan:
			l.Log().WithField(l.From, "publisher").Trace("got new outgoing message")
			msg := zmq4.NewMsgFrom([]byte(tMsg.Topic), []byte(tMsg.Content))
			err = (*pub).Send(msg)
			if err != nil {
				return fmt.Errorf("unable to send message: %w", err)
			}
			l.Log().WithField(l.From, "publisher").Trace("message sent")

		case <-ctx.Done():
			l.Log().WithField(l.From, "publisher").Info("cancelled, closing")
			return nil
		}
	}
}

func (p Publisher) dialPub(ctx context.Context) (*zmq4.Socket, error) {
	pub := zmq4.NewPub(ctx)

	addr := fmt.Sprintf("tcp://%s:%s", p.ProxyEndpoint, p.Port)

	l.Log().WithField(l.From, "publisher").Infof("dialing proxy %s", addr)

	err := pub.Dial(addr)
	if err != nil {
		err = fmt.Errorf("unable to dial proxy: %w", err)
		return nil, err
	} else {
		l.Log().WithField(l.From, "publisher").Infof("dialed proxy %s", addr)
	}

	return &pub, nil
}
