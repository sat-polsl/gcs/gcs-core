package io_test

import (
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/io"
)

func TestNewSubscriber(t *testing.T) {
	sub := io.NewSubscriber()

	if sub.Port != io.DefaultSubPort {
		t.Errorf("wrong value of Port, got: %s, wanted: %s", sub.Port, io.DefaultSubPort)
	}

	if sub.ProxyEndpoint != io.DefaultProxyEndpoint {
		t.Errorf("wrong value of ProxyEndpoint, got: %s, wanted: %s", sub.Port, io.DefaultProxyEndpoint)
	}

	if len(sub.Topics) < len(io.DefaultTopics) {
		t.Errorf("no topics will be subscribed, len(Topics) = %d", len(sub.Topics))
	}

	for _, dt := range io.DefaultTopics {
		hit := false
		for _, t := range sub.Topics {
			if t == dt {
				hit = true
				break
			}
		}

		if !hit {
			t.Errorf("missing topic: %s", dt)
		}
	}
}
