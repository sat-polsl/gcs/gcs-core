package io

import (
	"context"
	"fmt"
	"os"
	"sync"

	"github.com/go-zeromq/zmq4"
	"github.com/sirupsen/logrus"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type Subscriber struct {
	Topics        []string
	ProxyEndpoint string
	Port          string
}

func NewSubscriber() *Subscriber {
	proxyEndpoint := os.Getenv(EnvProxyEndpoint)
	if len(proxyEndpoint) < 1 {
		proxyEndpoint = DefaultProxyEndpoint
		l.Log().WithFields(logrus.Fields{
			l.From:           "subscriber",
			"proxy_endpoint": proxyEndpoint,
		}).Warn("default proxy endpoint not set, falling back to default")
	}

	port := os.Getenv(EnvSubscriberPort)
	if len(port) < 1 {
		port = DefaultSubPort
		l.Log().WithFields(logrus.Fields{
			l.From:     "subscriber",
			"sub_port": port,
		}).Warn("default pub port not set, falling back to default")
	}

	return &Subscriber{
		ProxyEndpoint: proxyEndpoint,
		Port:          port,
		Topics:        DefaultTopics,
	}
}

func (s Subscriber) String() string {
	return "subscirber"
}

func (s Subscriber) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	sub, err := s.dialSub(ctx)
	if err != nil {
		return err
	}
	defer (*sub).Close()

	go recvAndServe(sub, ipc)

	for {
		select {
		case <-ctx.Done():
			l.Log().WithField(l.From, "subsciber").Info("cancelled, closing")
			return nil
		}
	}
}

func recvAndServe(sub *zmq4.Socket, ipc *threads.IPC) {
	defer func() {
		if r := recover(); r != nil {
			l.Log().WithField(l.From, "recvAndServe").Logf(logrus.FatalLevel, "recovered from panic: %v", r)
			l.Fatal()
		}
	}()

	for {
		m, err := (*sub).Recv()
		if err != nil {
			l.Log().WithField(l.From, "recvAndServe").Errorf("unable to receive message: %s", err.Error())
		}

		if len(m.Frames) < 1 {
			l.Log().WithField(l.From, "recvAndServe").Errorf("frame had size of %d", len(m.Frames))
			continue
		}

		l.Log().WithField(l.From, "recvAndServe").Tracef("recieved message from %s", string(m.Frames[0]))

		ipcChan, err := ipc.Get(string(m.Frames[0]))
		if err != nil {
			l.Log().WithField(l.From, "recvAndServe").Errorf("topic %s cannot be served: %s", string(m.Frames[0]), err.Error())
			continue
		}
		tm := threads.NewMessage(m.Frames)

		l.Log().WithFields(logrus.Fields{
			l.From:          "recvAndServe",
			"frame-channel": string(m.Frames[0]),
		}).Tracef("sending recieved message")
		ipcChan <- tm

		if string(m.Frames[0]) == threads.SubRotorChannel {
			ipcStorageChan, err := ipc.Get(threads.StorageChannel)
			if err != nil {
				l.Log().WithField(l.From, "recvAndServe").Errorf("cannot send data to storage channel: %s", err.Error())
				continue
			}

			l.Log().WithField(l.From, "recvAndServe").Trace("sending recieved message to storage channel")
			ipcStorageChan <- tm
		}

		l.Log().WithField(l.From, "recvAndServe").Trace("message served")

	}
}

func (s Subscriber) dialSub(ctx context.Context) (*zmq4.Socket, error) {
	sub := zmq4.NewSub(ctx)

	addr := fmt.Sprintf("tcp://%s:%s", s.ProxyEndpoint, s.Port)
	err := sub.Dial(addr)
	if err != nil {
		err = fmt.Errorf("unable to dial proxy: %w", err)
		return nil, err
	} else {
		l.Log().WithField(l.From, "subscriber").Infof("dialed proxy: %s", addr)
	}

	for _, t := range s.Topics {
		err = sub.SetOption(zmq4.CmdSubscribe, t)
		if err != nil {
			return nil, fmt.Errorf("unable to set topic (%s): %w", t, err)
		}
	}

	return &sub, nil
}
