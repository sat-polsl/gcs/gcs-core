package io_test

import (
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/io"
)

func TestNewPublisher(t *testing.T) {
	pub := io.NewPublisher()

	if pub.Port != io.DefaultPubPort {
		t.Errorf("wrong value of Port, got: %s, wanted: %s", pub.Port, io.DefaultPubPort)
	}

	if pub.ProxyEndpoint != io.DefaultProxyEndpoint {
		t.Errorf("wrong value of ProxyEndpoint, got: %s, wanted: %s", pub.Port, io.DefaultProxyEndpoint)
	}
}
