package rotor_test

import (
	"math"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/rotor"
)

var TestCoordinateGS = rotor.Coordinate{
	Altitude:  0.0,
	Latitude:  50.2934,
	Longitude: 18.682,
}

var TestCoordinateSpacecraft = rotor.Coordinate{
	Altitude:  10951.94,
	Latitude:  50.4915,
	Longitude: 18.7102,
}

var (
	// ExpectedDownrange is a horizontal distance between point A and point B
	ExpectedDownrange       = 22118.24029
	DownrangePrecisionError = 0.001

	// ExpectedDistance is a vertical distance between point A and point B
	ExpectedDistance       = 24698.21914
	DistancePrecisionError = 0.001

	// ExpectedAzimuth is an arc of the horizon measured between the true north and the point B
	// passing through the center of a point A, clockwise from the north point
	ExpectedAzimuth       = 5.17471
	AzimuthPrecisionError = 0.001

	// ExpectedElevation is the angular distance of point B above the point A
	ExpectedElevation       = 26.22346
	ElevationPrecisionError = 0.001
)

func TestConvert(t *testing.T) {
	m2c := rotor.Convert(1, rotor.Meter, rotor.Centimeter)
	if m2c != 100.0 {
		t.Errorf("conversion error: got %f, wanted 100", m2c)
	}

	m2km := rotor.Convert(1000, rotor.Meter, rotor.Kilometer)
	if m2km != 1.0 {
		t.Errorf("conversion error: got %f, wanted 100", m2km)
	}
}

func TestCoordinateCalculateRotorFrame(t *testing.T) {
	// smoke test
	rf := TestCoordinateGS.CalculateRotorFrame(TestCoordinateSpacecraft)

	if actualError := math.Abs(rf.Downrange-ExpectedDownrange) / ExpectedDownrange; actualError > DownrangePrecisionError {
		t.Errorf("downrange: precission error: expected error <= %f, got %f", DownrangePrecisionError, actualError)
	} else {
		t.Logf("downrange: obtained error: %f", actualError)
	}

	if actualError := math.Abs(rf.Distance-ExpectedDistance) / ExpectedDistance; actualError > DistancePrecisionError {
		t.Errorf("distance: precission error: expected error <= %f, got %f", DistancePrecisionError, actualError)
	} else {
		t.Logf("distance: obtained error: %f", actualError)
	}

	if actualError := math.Abs(rf.Azimuth-ExpectedAzimuth) / ExpectedAzimuth; actualError > AzimuthPrecisionError {
		t.Errorf("azimuth: precission error: expected error <= %f, got %f", AzimuthPrecisionError, actualError)
	} else {
		t.Logf("azimuth: obtained error: %f", actualError)
	}

	if actualError := math.Abs(rf.Elevation-ExpectedElevation) / ExpectedElevation; actualError > ElevationPrecisionError {
		t.Errorf("elevation: precission error: expected error <= %f, got %f", ElevationPrecisionError, actualError)
	} else {
		t.Logf("elevation: obtained error: %f", actualError)
	}

	t.Logf("RotorFrame: %+v", rf)
}
