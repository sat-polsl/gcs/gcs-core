package rotor

// Constants containing names of env variables
const (
	EnvRotorAltitude  = "ROTOR_ALTITUDE"
	EnvRotorLatitude  = "ROTOR_LATITUDE"
	EnvRotorLongitude = "ROTOR_LONGITUDE"
)

// Default values for rotor placed on roof of Centrum Nowych Technologii at the
// Silesian University of Technology.
//
// See also:
//
// * https://www.polsl.pl/rju3-cnt
const (
	DefaultAltitude  = float64(217 + 14)
	DefaultLatitude  = float64(50.293436)
	DefaultLongitude = float64(18.682064)
)

type unit float64

const (
	Kilometer  = unit(1)
	Meter      = unit(Kilometer * 1000)
	Centimeter = unit(Meter * 100)
)

// EarthRadius approximation of Earth Radius in given units
func EarthRadius(u unit) float64 {
	return 6371.0 * float64(u)
}
