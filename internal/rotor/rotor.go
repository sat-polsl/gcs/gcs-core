// Package rotor implements the rotor controller thread.
package rotor

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type RotorFrame struct {
	Downrange float64 `json:"downrange"`
	Distance  float64 `json:"distance"`
	Azimuth   float64 `json:"azimuth"`
	Elevation float64 `json:"elevation"`
}

type Rotor struct {
	rotorCoordinate Coordinate
}

func New() *Rotor {
	alt, err := strconv.ParseFloat(os.Getenv(EnvRotorAltitude), 64)
	if err != nil {
		alt = DefaultAltitude

		l.Log().WithField(l.From, "rotor").Warnf("unable to get Rotor Altitude, falling back to default (%f): %s", alt, err.Error())
	}

	lat, err := strconv.ParseFloat(os.Getenv(EnvRotorLatitude), 64)
	if err != nil {
		lat = DefaultLatitude

		l.Log().WithField(l.From, "rotor").Warnf("unable to get Rotor Latitude, falling back to default (%f): %s", lat, err.Error())
	}

	long, err := strconv.ParseFloat(os.Getenv(EnvRotorLongitude), 64)
	if err != nil {
		long = DefaultLongitude

		l.Log().WithField(l.From, "rotor").Warnf("unable to get Rotor Longitude, falling back to default (%f): %s", long, err.Error())
	}

	return &Rotor{
		rotorCoordinate: Coordinate{
			Altitude:  alt,
			Longitude: long,
			Latitude:  lat,
		},
	}
}

func (r Rotor) Marshall(rf RotorFrame) ([]byte, error) {
	return json.Marshal(rf)
}

func (r Rotor) Unmarshall(content []byte, cord *Coordinate) error {
	return json.Unmarshal(content, cord)
}

func (r Rotor) String() string {
	return "rotor"
}

func (r Rotor) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	ipcRecvChan, err := ipc.Get(threads.SubRotorChannel)
	if err != nil {
		return fmt.Errorf("unable to get storage channel: %w", err)
	}

	for {
		select {
		case msg := <-ipcRecvChan:
			l.Log().WithField(l.From, "rotor").Trace("received message")
			var frame Coordinate
			if err := r.Unmarshall(msg.Content, &frame); err != nil {
				l.Log().WithField(l.From, "rotor").Errorf("unable to unmarshall frame: %s", err.Error())
				continue
			}

			l.Log().WithField(l.From, "rotor").Trace("calculating rotor frame")
			rf := r.rotorCoordinate.CalculateRotorFrame(frame)

			l.Log().WithField(l.From, "rotor").Trace("marshalling frame")
			b, err := r.Marshall(rf)
			if err != nil {
				l.Log().WithField(l.From, "rotor").Warnf("unable to marshall frame: %s", err.Error())
				continue
			}

			outMsg := threads.NewMessage([][]byte{[]byte(threads.PubRotorChanel), b})
			l.Log().WithFields(logrus.Fields{
				l.From:          "rotor",
				"frame-channel": outMsg.Topic,
			}).Trace("sending message")

			ipcSendChan, err := ipc.Get(outMsg.Topic)
			if err != nil {
				l.Log().WithField(l.From, "rotor").Errorf("unable to send frame: %s", err.Error())
				continue
			}

			ipcSendChan <- outMsg

			l.Log().WithField(l.From, "rotor").Trace("processed message")

		case <-ctx.Done():
			l.Log().WithField(l.From, "rotor").Info("cancelled, closing")
			return nil
		}
	}
}
