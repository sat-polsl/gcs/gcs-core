package rotor_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/rotor"
)

var TestCoordinate = rotor.Coordinate{
	Altitude:  10,
	Latitude:  10,
	Longitude: 10,
}

var TestRotorFrame = rotor.RotorFrame{
	Azimuth:   10,
	Elevation: 10,
	Downrange: 10,
	Distance:  10,
}

func TestRotorMarshall(t *testing.T) {
	if _, err := rotor.New().Marshall(TestRotorFrame); err != nil {
		t.Errorf("marshall failed: %s", err.Error())
	}
}

func TestRotorUnmarshall(t *testing.T) {
	b, err := json.Marshal(TestCoordinate)
	if err != nil {
		t.Errorf("marshall failed: %s", err.Error())
	}

	var c rotor.Coordinate
	if err = rotor.New().Unmarshall(b, &c); err != nil {
		t.Errorf("marshall failed: %s", err.Error())
	}

	if c.Altitude != TestCoordinate.Altitude || c.Latitude != TestCoordinate.Latitude || c.Longitude != TestCoordinate.Longitude {
		t.Errorf("contents not equal: got %+v, wanted %+v", c, TestCoordinate)
	}
}
