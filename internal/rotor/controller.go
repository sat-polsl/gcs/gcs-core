package rotor

import m "math"

// Coordinate is a point in the real world.
type Coordinate struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Altitude  float64 `json:"altitude"`
}

func Convert(distance float64, from unit, to unit) float64 {
	return distance * float64(to) / float64(from)
}

func deg2rad(deg float64) float64 {
	return deg * m.Pi / 180.0
}

func rad2deg(rad float64) float64 {
	return rad * 180.0 / m.Pi
}

// CalculateRotorFrame calculates rotor settings for current spacecraft position
// as determined by coord.
func (crd Coordinate) CalculateRotorFrame(coord Coordinate) (rf RotorFrame) {
	φ1 := deg2rad(crd.Latitude)
	φ2 := deg2rad(coord.Latitude)

	λ1 := deg2rad(crd.Longitude)
	λ2 := deg2rad(coord.Longitude)

	Δφ := φ2 - φ1
	Δλ := λ2 - λ1

	a := m.Pow(m.Sin(Δφ/2.0), 2) +
		m.Cos(φ1)*m.Cos(φ2)*
			m.Pow(m.Sin(Δλ/2.0), 2)

	c := 2.0 * m.Atan2(m.Sqrt(a), m.Sqrt(1.0-a))

	rf.Downrange = EarthRadius(Meter) * c

	rf.Distance = m.Sqrt(m.Pow(EarthRadius(Meter), 2) + m.Pow(EarthRadius(Meter)+coord.Altitude, 2) - 2*EarthRadius(Meter)*(EarthRadius(Meter)+coord.Altitude)*m.Cos(c))

	rf.Azimuth = rad2deg(m.Atan2(m.Sin(Δλ)*m.Cos(φ2), m.Cos(φ1)*m.Sin(φ2)-m.Sin(φ1)*m.Cos(φ2)*m.Cos(Δλ)))

	rf.Elevation = m.Acos(-(2*coord.Altitude*EarthRadius(Meter)+m.Pow(coord.Altitude, 2)-m.Pow(rf.Distance, 2))/(2*rf.Distance*EarthRadius(Meter)))*180.0/m.Pi - 90

	return
}
