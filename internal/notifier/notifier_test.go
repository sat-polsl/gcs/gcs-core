package notifier_test

import (
	"encoding/json"
	"errors"
	"os"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/notifier"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
	"gitlab.com/sat-polsl/gcs/gcs-core/mail"
)

var example = map[string]interface{}{
	"string": "string",
	"int":    1,
	"float":  2.3,
	"null":   nil,
	"array":  []string{"el0", "el1", "el2"},
}

func TestMain(m *testing.M) {
	if err := tools.SetEnvs(tools.EnvVar{
		mail.EnvAllowedDomains: os.Getenv("TEST_ALLOWED_DOMAINS"),
		mail.EnvSMTPEmail:      os.Getenv("TEST_EMAIL_ADDRESS"),
		mail.EnvSMTPPassword:   os.Getenv("TEST_SMTP_PASSWORD"),
		mail.EnvSMTPUser:       os.Getenv("TEST_SMTP_USER"),
		mail.EnvSMTPHost:       os.Getenv("TEST_SMTP_HOST"),
		mail.EnvSMTPPort:       os.Getenv("TEST_SMTP_PORT"),
		mail.EnvSMTPSecure:     os.Getenv("TEST_SMTP_SECURE"),
	}); err != nil {
		os.Exit(1)
	}

	// run tests
	code := m.Run()

	os.Exit(code)
}

func TestNew(t *testing.T) {
	n := notifier.New()

	if len(n.Recipients) == 0 {
		t.Errorf("wrong number of recipients, expected at least 1, got %d", len(n.Recipients))
	}
}

func TestMakeBody(t *testing.T) {
	n := notifier.New()

	b, err := json.Marshal(example)
	if err != nil {
		t.Errorf("marshalling example failed: %s", err.Error())
	}

	if _, err := n.MakeBody(b); err != nil {
		t.Errorf("unable to create body: %s", err.Error())
	}

	if _, err := n.MakeBody([]byte{0, 1, 2, 3}); err != nil && !errors.Is(err, notifier.ErrIndentationFailed{}) {
		t.Errorf("wrong type of error, got %s, wanted %s", err.Error(), notifier.ErrIndentationFailed{}.Error())
	}
}
