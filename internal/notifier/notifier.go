// Package notifier is responsible for sending e-mails in case of critical
// hardware failures.
package notifier

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
	"gitlab.com/sat-polsl/gcs/gcs-core/mail"
)

type Notifier struct {
	client     mail.Client
	Recipients []string
}

func New() *Notifier {
	client := mail.NewClient()

	defaultRecipient := client.Email

	return &Notifier{
		client:     client,
		Recipients: []string{defaultRecipient},
	}
}

func (n Notifier) String() string {
	return "notifier"
}

func (n Notifier) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	l.Log().WithField(l.From, "notifier").Info("starting")

	ipcRecvChan, err := ipc.Get(threads.CriticalNotificationChanel)
	if err != nil {
		return fmt.Errorf("unable to get storage channel: %w", err)
	}

	for {
		select {
		case msg := <-ipcRecvChan:
			l.Log().WithField(l.From, "notifier").Infof("got critical error notification: %s", msg.Topic)

			body, err := n.MakeBody(msg.Content)
			if errors.Is(err, ErrIndentationFailed{}) {
				l.Log().WithField(l.From, "notifier").Errorf("unable to create body: %s", err.Error())
			} else if err != nil {
				body = string(msg.Content)

				l.Log().WithField(l.From, "notifier").Errorf("unable to create body, overwriting message with string([]byte), error: %s", err.Error())
			}

			n.client.SendMail(
				n.Recipients,
				fmt.Sprintf(NotificationSubject, msg.Topic),
				body,
			)

		case <-ctx.Done():
			l.Log().WithField(l.From, "notifier").Info("cancelled, closing")
			return nil
		}
	}
}

func (n Notifier) MakeBody(content []byte) (string, error) {
	var data map[string]interface{}
	json.Unmarshal(content, &data)

	b, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return fmt.Sprintf(NotificationBody, string(content)), ErrIndentationFailed{
			Other: err,
		}
	}

	return fmt.Sprintf(NotificationBody, string(b)), nil
}
