package notifier

import (
	_ "embed"
	"fmt"
)

const (
	NotificationSubject = "[CRITICAL] Error recieved from GCS on %s"
)

type ErrIndentationFailed struct {
	Other error
}

func (err ErrIndentationFailed) Error() string {
	if err.Other != nil {
		return fmt.Sprintf("unable to create indentation: %s", err.Other.Error())
	} else {
		return "unable to create indentation"
	}
}

//go:embed templates/notification.txt
var NotificationBody string
