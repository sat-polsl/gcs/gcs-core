package serializer

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

type Serializer struct {
	IndentSize int
}

var defaultSerializer = Serializer{
	IndentSize: 2,
}

func Marshal(data [][]byte) (out []byte, err error) {
	if defaultSerializer.MaxDepth(data) == 1 {
		// TODO:
		// return defaultSerializer.ToCSV(data)
		return defaultSerializer.ToJSONArray(data)
	} else {
		return defaultSerializer.ToJSONArray(data)
	}
}

func (s Serializer) Marshal(data [][]byte) (out []byte, err error) {
	if s.MaxDepth(data) == 1 {
		// TODO:
		// return s.ToCSV(data)
		return s.ToJSONArray(data)
	} else {
		return s.ToJSONArray(data)
	}
}

func (s Serializer) MaxDepth(data [][]byte) int {
	maxDepth := 0

	for _, b := range data {
		depth := 0

		for _, r := range string(b) {
			switch r {
			case '[':
				fallthrough
			case '{':
				depth++
			case ']':
				fallthrough
			case '}':
				depth--
			}

			if depth > maxDepth {
				maxDepth = depth
			}
		}
	}

	return maxDepth
}

func (s Serializer) ToCSV(data [][]byte) (out []byte, err error) {
	// TODO:: Implement
	// for _, b := range data {
	// 	if err = json.Unmarshal(b,); err != nil {
	// 		err = fmt.Errorf("unable to unmarshall data: %w", err)
	// 		return
	// 	}
	// }

	var writer strings.Builder
	csv.NewWriter(&writer)

	return []byte(writer.String()), nil
}

func (s Serializer) prettifyJSON(data [][]byte) (out [][]byte, err error) {
	j := make(map[string]interface{})

	for _, b := range data {
		if err = json.Unmarshal(b, &j); err != nil {
			err = fmt.Errorf("unable to unmarshall data: %w", err)
			return
		}

		b, err = json.MarshalIndent(j, "", strings.Repeat(" ", s.IndentSize))
		if err != nil {
			err = fmt.Errorf("unable to marshall data with indentation: %w", err)
			return
		}

		out = append(out, b)
	}

	return
}

type wrapper struct {
	Array []string
}

func (w wrapper) Fill(data [][]byte) wrapper {
	var sData []string
	for _, b := range data {
		sData = append(sData, string(b))
	}

	w.Array = sData

	return w
}

func (s Serializer) RemoveLast(data []byte, rm rune) (out []byte) {
	rev := tools.Reverse(string(data))
	for i, r := range rev {
		if r == rm {
			rev = tools.Reverse(string(append([]rune(rev[0:i]), []rune(rev[i+1:])...)))
			return []byte(rev)
		}
	}
	return []byte(tools.Reverse(rev))
}

func (s Serializer) ToJSONArray(data [][]byte) (out []byte, err error) {
	if s.IndentSize > 0 {
		data, err = s.prettifyJSON(data)
		if err != nil {
			err = fmt.Errorf("unable to prettify data: %w", err)
			return
		}
	}

	td := wrapper{}.Fill(data)

	t, err := template.New("x").Parse("[\n{{range .Array}}{{.}},\n{{end}}]")
	if err != nil {
		err = fmt.Errorf("unable to create template: %w", err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, td)
	if err != nil {
		err = fmt.Errorf("unable to execute template: %w", err)
		return
	}

	return s.RemoveLast(tpl.Bytes(), ','), nil
}
