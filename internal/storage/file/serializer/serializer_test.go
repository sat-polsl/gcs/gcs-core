package serializer_test

import (
	"encoding/json"
	"testing"
	"time"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/file/serializer"
)

var (
	serializerData = map[string]interface{}{
		"spacecraft_id": 1,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}

	depth2Data = map[string]interface{}{
		"spacecraft_id": 1,
		"timestamp":     time.Now().Unix(),
		"position": map[string]interface{}{
			"latitude":  1.1,
			"longitude": 2.2,
			"altitude":  3.3,
		},
	}
)

func TestToJSONArray(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(serializerData)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	_, err = s.ToJSONArray([][]byte{b})
	if err != nil {
		t.Errorf("unable to convert data to JSON array: %s", err.Error())
	}
}

func TestMaxDepth(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(depth2Data)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	depth := s.MaxDepth([][]byte{b})
	if depth != 2 {
		t.Errorf("depth calculation error: got %d, wanted: %d", depth, 2)
	}
}

func TestToCSV(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(serializerData)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	_, err = s.ToCSV([][]byte{b})
	if err != nil {
		t.Errorf("unable to convert data to JSON array: %s", err.Error())
	}
}
