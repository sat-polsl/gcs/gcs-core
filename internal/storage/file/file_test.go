package file_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"
	"time"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/file"
)

var (
	pathPrefix     = "tmp"
	numberOfShards = 4

	fileData = map[string]interface{}{
		"spacecraft_id": 1.0,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}
)

func TestMain(m *testing.M) {
	os.Setenv(file.EnvEmergencyContentDump, "true")

	// run tests
	code := m.Run()

	// cleanup after tests
	if err := os.RemoveAll(pathPrefix); err != nil {
		fmt.Printf("warning: %s\n", err.Error())
	}

	os.Exit(code)
}

func TestNewClient(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClientWithOpts(ctx, file.ClientOpts{})
	if err := fc.Close(ctx); err != nil {
		t.Errorf("error during closing FileClient: %s", err.Error())
	}
}

func TestSave(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClientWithOpts(ctx, file.ClientOpts{
		PathPrefix: pathPrefix,
	})

	if err := fc.Save(ctx, fileData); err != nil {
		t.Errorf("error during saving content to data field: %s", err.Error())
	}

	b := fc.Get(int(fileData["spacecraft_id"].(float64)))[0]

	marshaled, err := json.Marshal(fileData)
	if err != nil {
		t.Errorf("error during marshalling content: %s", err.Error())
	}

	if !bytes.Equal(b, marshaled) {
		t.Errorf("assertion failed, got: %s, wanted: %s", marshaled, fileData)
	}
}

func TestClose(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClientWithOpts(ctx, file.ClientOpts{
		PathPrefix: pathPrefix,
	})

	for i := 0; i < 10; i++ {
		if err := fc.Save(ctx, fileData); err != nil {
			t.Errorf("error during saving content to data field: %s", err.Error())
		}
	}

	err := fc.Close(ctx)
	if err != nil {
		t.Errorf("error during closing: %s", err.Error())
	}

	stat, err := os.Stat(pathPrefix)
	if err != nil {
		t.Errorf("error during checking output: %s", err.Error())
	}

	if !stat.IsDir() {
		t.Errorf("error, %s should be a directory", pathPrefix)
	}
}

func TestShard(t *testing.T) {
	pathPrefixSharding := path.Join(pathPrefix, "sharding")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClientWithOpts(ctx, file.ClientOpts{
		PathPrefix: pathPrefixSharding,
	})

	marshaled, err := json.Marshal(fileData)
	if err != nil {
		t.Errorf("error during marshalling content: %s", err.Error())
	}

	for i := 0; i < (numberOfShards*4*file.SizeMegabyte)/len(marshaled); i++ {
		if err := fc.Save(ctx, fileData); err != nil {
			t.Errorf("error during saving content to data field: %s", err.Error())
		}
	}

	err = fc.Close(ctx)
	if err != nil {
		t.Errorf("error during closing: %s", err.Error())
	}

	stat, err := os.Stat(pathPrefixSharding)
	if err != nil {
		t.Errorf("error during checking output: %s", err.Error())
	}

	if !stat.IsDir() {
		t.Errorf("error, %s should be a directory", pathPrefixSharding)
	}

	f, err := ioutil.ReadDir(pathPrefixSharding)
	if err != nil {
		t.Errorf("unable to read contents of the directory: %s", err.Error())
	}

	if len(f) != numberOfShards {
		t.Errorf("wrong number of shards, got: %d, wanted: %d", len(f), numberOfShards)
	}
}
