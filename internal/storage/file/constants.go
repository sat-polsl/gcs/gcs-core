package file

import "fmt"

// Constants containing names of ENV variables
const (
	EnvDataFilesLocation    = "DATA_FILES_LOCATION"
	EnvDataFilesExtension   = "DATA_FILES_EXTENSION"
	EnvEmergencyContentDump = "EMERGENCY_CONTENT_DUMP"
	EnvShardSize            = "DUMP_FILE_SHARD_SIZE"
)

const (
	ConnectionString            = ""
	DefaultEmergencyContentDump = false
	DefaultPathPrefix           = "data_dump"
	DefaultFileExtension        = "txt"
	DefaultShardSize            = 4 * SizeMegabyte
)

const (
	SizeByte     = 1
	SizeKilobyte = 1000 * SizeByte
	SizeMegabyte = 1000 * SizeKilobyte
	SizeGgabyte  = 1000 * SizeMegabyte
)

type ErrNotSupported struct {
	Backend string
}

func (err ErrNotSupported) Error() string {
	return fmt.Sprintf("%s does not support this action, deploy the app with another storage backend", err.Backend)
}
