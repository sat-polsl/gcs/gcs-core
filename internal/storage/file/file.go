package file

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/file/serializer"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

// Client
type Client struct {
	contentDump   bool
	data          map[int][][]byte
	mux           sync.Mutex
	pathPrefix    string
	fileExtension string
	shardSize     int64
}

type ClientOpts struct {
	PathPrefix    string
	FileExtension string
}

func NewClient(ctx context.Context) *Client {
	return NewClientWithOpts(ctx, ClientOpts{
		FileExtension: os.Getenv(EnvDataFilesExtension),
		PathPrefix:    os.Getenv(EnvDataFilesLocation),
	})
}

// NewClientWithOpts
func NewClientWithOpts(ctx context.Context, opts ClientOpts) *Client {
	sDump := os.Getenv(EnvEmergencyContentDump)

	if len(opts.FileExtension) == 0 {
		l.Log().WithFields(logrus.Fields{
			l.From:           "file-client",
			"file_extension": DefaultFileExtension,
		}).Warnf("emergency content dump not set, falling back to default")
		opts.FileExtension = DefaultFileExtension
	}

	if len(opts.PathPrefix) == 0 {
		l.Log().WithFields(logrus.Fields{
			l.From:        "file-client",
			"path_prefix": DefaultPathPrefix,
		}).Warnf("emergency content dump not set, falling back to default")
		opts.PathPrefix = DefaultPathPrefix
	}

	var err error
	var dump bool
	if len(sDump) == 0 {
		l.Log().WithFields(logrus.Fields{
			l.From:                   "file-client",
			"emergency_content_dump": DefaultEmergencyContentDump,
		}).Warnf("emergency content dump not set, falling back to default")

		dump = false
	} else {
		dump, err = strconv.ParseBool(sDump)
		if err != nil {
			l.Log().WithFields(logrus.Fields{
				l.From:                   "file-client",
				"value":                  sDump,
				"emergency_content_dump": DefaultEmergencyContentDump,
			}).Errorf("parsing %s failed, falling back to default", EnvEmergencyContentDump)

			dump = false
		}
	}

	sShardSize := os.Getenv(EnvShardSize)
	var shardSize int64
	if len(sShardSize) == 0 {
		l.Log().WithFields(logrus.Fields{
			l.From:                 "file-client",
			"dump_file_shard_size": DefaultShardSize,
		}).Warnf("dump file shard size not set, falling back to default")

		shardSize = DefaultShardSize
	} else {
		shardSize, err = strconv.ParseInt(sShardSize, 10, 0)
		if err != nil {
			l.Log().WithFields(logrus.Fields{
				l.From:                 "file-client",
				"value":                sShardSize,
				"dump_file_shard_size": DefaultShardSize,
			}).Errorf("parsing %s failed, falling back to default", EnvShardSize)

			shardSize = DefaultShardSize
		} else {
			shardSize = shardSize * SizeMegabyte
		}
	}

	return &Client{
		data:          make(map[int][][]byte),
		contentDump:   dump,
		shardSize:     shardSize,
		fileExtension: opts.FileExtension,
		pathPrefix:    opts.PathPrefix,
	}
}

// Get
func (c *Client) Get(spacecraftID int) [][]byte {
	return c.data[spacecraftID]
}

// Save
func (c *Client) Save(ctx context.Context, m map[string]interface{}) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	b, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("unable to marshall data into JSON: %w", err)
	}

	l.Log().WithFields(logrus.Fields{
		l.From:      "file-client-save",
		"data_size": len(b),
	}).Trace("marshalled data")

	fID := m["spacecraft_id"].(float64)

	c.data[int(fID)] = append(c.data[int(fID)], b)

	return nil
}

// Shard
func (c *Client) Shard(data [][]byte) (out [][][]byte) {
	dataSize := int64(0)
	chunkBeginning := 0
	for i, b := range data {
		dataSize += int64(len(b))
		if dataSize >= c.shardSize {
			dataSize = 0
			l.Log().WithFields(logrus.Fields{
				l.From:      "shard",
				"data_size": dataSize,
			}).Trace("sharding in progres, creating chunk")

			out = append(out, data[chunkBeginning:i-1])
			chunkBeginning = i - 1
		}
	}

	if chunkBeginning < len(data) {
		out = append(out, data[chunkBeginning:])
	}

	if len(out) == 0 {
		out = append(out, data)
	}

	return
}

// Dump
func (c *Client) Dump() (errs []error) {
	c.mux.Lock()
	defer c.mux.Unlock()

	l.Log().WithFields(logrus.Fields{
		l.From:           "file-client-dump",
		"path_prefix":    c.pathPrefix,
		"file_extension": c.fileExtension,
	}).Info("dumping files")

	for spacecraftID, data := range c.data {
		l.Log().WithFields(logrus.Fields{
			l.From:          "file-client-dump",
			"spacecraft_id": spacecraftID,
		}).Trace("dumping files")

		if stat, err := os.Stat(c.pathPrefix); errors.Is(err, os.ErrNotExist) {
			if err := os.MkdirAll(c.pathPrefix, 0764); err != nil {
				errs = append(errs, err)
				if c.contentDump {
					l.Log().WithFields(logrus.Fields{
						l.From:          "file-client-dump",
						"spacecraft_id": spacecraftID,
						"contents":      string(concat(data)),
					}).Error("emergency content dump")
				}
				continue
			}
		} else if !stat.IsDir() {
			errs = append(errs, fmt.Errorf("%s is not a directory", c.pathPrefix))
			if c.contentDump {
				l.Log().WithFields(logrus.Fields{
					l.From:          "file-client-dump",
					"spacecraft_id": spacecraftID,
					"contents":      string(concat(data)),
				}).Error("emergency content dump")
			}
			continue
		}

		l.Log().WithField(l.From, "file-client-dump").Tracef("starting sharding")
		sharded := c.Shard(data)
		l.Log().WithFields(logrus.Fields{
			l.From:   "file-client-dump",
			"shards": len(sharded),
		}).Tracef("sharding done")
		for _, s := range sharded {
			fname := path.Join(c.pathPrefix, fmt.Sprintf("%d-%d.%s",
				spacecraftID,
				time.Now().UnixMilli(),
				c.fileExtension))

			b, err := serializer.Marshal(s)
			if err != nil {
				errs = append(errs, err)
				if c.contentDump {
					l.Log().WithFields(logrus.Fields{
						l.From:          "file-client-dump",
						"spacecraft_id": spacecraftID,
						"filename":      fname,
						"contents":      string(concat(s)),
					}).Error("emergency content dump")
				}
				continue
			}

			l.Log().WithFields(logrus.Fields{
				l.From:      "file-client-dump",
				"data-size": len(b),
			}).Trace("writing data")
			err = os.WriteFile(fname, b, 0666)
			if err != nil {
				errs = append(errs, err)
				if c.contentDump {
					l.Log().WithFields(logrus.Fields{
						l.From:          "file-client-dump",
						"spacecraft_id": spacecraftID,
						"filename":      fname,
						"contents":      string(concat(s)),
					}).Error("emergency content dump")
				}
				continue
			}
		}

		c.data[spacecraftID] = [][]byte{}
	}

	return errs
}

func concat(bbs [][]byte) (out []byte) {
	for _, bs := range bbs {
		out = append(out, bs...)
		out = append(out, []byte("\n")...)
	}

	return out
}

// Close
func (c *Client) Close(ctx context.Context) error {
	l.Log().WithField(l.From, "file-client").Info("closing")

	errs := c.Dump()
	if len(errs) != 0 {
		return fmt.Errorf("one or more errors received during dumping the data: %v", errs)
	}

	return nil
}

func (c *Client) ListMissions(context.Context) ([]map[string]string, error) {
	return nil, &ErrNotSupported{
		Backend: "file",
	}
}

func (c *Client) MissionInfo(context.Context, string) (map[string]interface{}, error) {
	return nil, &ErrNotSupported{
		Backend: "file",
	}
}

func (c *Client) CreateMission(context.Context, string, string, map[string]string) error {
	return &ErrNotSupported{
		Backend: "file",
	}
}

func (c *Client) UpdateMission(context.Context, string, string) error {
	return &ErrNotSupported{
		Backend: "file",
	}
}

func (c *Client) GetValues(ctx context.Context, id string) ([]map[string]interface{}, error) {
	return []map[string]interface{}{}, &ErrNotSupported{
		Backend: "file",
	}
}
