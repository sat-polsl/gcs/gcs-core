package vault

import "fmt"

const (
	EnvVaultToken   = "VAULT_TOKEN"
	EnvVaultAddress = "VAULT_ADDRESS"
)

const DefaultVaultAddress = "vault:8200"

var ErrBadToken = fmt.Errorf("bad token")
