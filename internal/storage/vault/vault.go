package vault

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type DynamicCredentials struct {
	Token string
}

func NewDynamicCredentials(token string) (DynamicCredentials, error) {
	if len(token) == 0 {
		return DynamicCredentials{}, ErrBadToken
	}
	return DynamicCredentials{Token: token}, nil
}

func (vault DynamicCredentials) Get(database string) (string, string, error) {
	resp, err := vault.SendRequest(database)
	if err != nil {
		return "", "", fmt.Errorf("unable to send request: %w", err)
	}

	if resp.StatusCode == http.StatusOK {
		return vault.ParseCredentials(resp)
	} else if resp.StatusCode == http.StatusForbidden {
		return "", "", fmt.Errorf("vault cannot identify your application, client token is invalid: %w", err)
	}

	return "", "", fmt.Errorf("unexpected error code: %d", resp.StatusCode)
}

func (vault DynamicCredentials) SendRequest(database string) (*http.Response, error) {
	client := &http.Client{}

	address := os.Getenv(EnvVaultAddress)
	if len(address) == 0 {
		address = DefaultVaultAddress
		l.Log().WithFields(logrus.Fields{
			"vault_address": address,
			l.From:          "vault-send-request",
		}).Warn("default proxy endpoint not set, falling back to default")
	}

	address = fmt.Sprintf("http://%s/v1/database/creds/%s", address, database)
	req, err := http.NewRequest("GET", address, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create request: %w", err)
	}

	req.Header.Add("X-Vault-Token", vault.Token)
	return client.Do(req)
}

func (vault DynamicCredentials) ParseCredentials(resp *http.Response) (string, string, error) {
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", "", fmt.Errorf("unable to read response: %w", err)
	}

	var VaultJSONResponse map[string]interface{}
	if err := json.Unmarshal(bodyBytes, &VaultJSONResponse); err != nil {
		return "", "", fmt.Errorf("unable to unmarshal json response: %w", err)
	}

	creds := VaultJSONResponse["data"].(map[string]interface{})
	username := creds["username"].(string)
	password := creds["password"].(string)

	l.Log().WithFields(logrus.Fields{
		"username":       username,
		"password":       password,
		"status code":    resp.StatusCode,
		"renewable":      VaultJSONResponse["renewable"],
		"lease_id":       VaultJSONResponse["lease_id"],
		"lease_duration": VaultJSONResponse["lease_duration"],
		l.From:           "parse-credentials",
	}).Info("successfully authenticated with Vault, and a MongoDB credential has been created")

	return username, password, nil
}
