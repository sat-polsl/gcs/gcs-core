// Package storage is implementing structure and all necessary substructures and
// interfaces that are intended to run as a thread saving (and possibly reading)
// data to/from the storage.
//
// Storage itself is abstracted behind the Backend interface - it can be text
// file (JSON or CSV?) or SQLite database.
package storage

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/file"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/postgres"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/threads"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type Backend interface {
	Save(context.Context, map[string]interface{}) error
	Close(context.Context) error
}

type MissionRegistry interface {
	ListMissions(context.Context) ([]map[string]string, error)
	MissionInfo(context.Context, string) (map[string]interface{}, error)
	GetValues(context.Context, string) ([]map[string]interface{}, error)
	CreateMission(context.Context, string, string, map[string]string) error
	UpdateMission(context.Context, string, string) error
	Close(context.Context) error
}

type BackendMissionRegistry interface {
	Backend
	MissionRegistry
}

type Storage struct {
	Backend Backend
}

type BackendRegistryFactory struct{}

func (bf BackendRegistryFactory) New() BackendMissionRegistry {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	postgresBackend, ok := bf.newPostgresBackend(ctx)
	fileBackend := bf.newFileBackend(ctx)

	var backend BackendMissionRegistry
	if ok {
		backend = postgresBackend
	} else {
		backend = fileBackend
	}

	return backend
}

func (bf BackendRegistryFactory) newPostgresBackend(ctx context.Context) (BackendMissionRegistry, bool) {
	b, err := postgres.New(ctx)
	if errors.Is(err, postgres.ErrNotConfigured{}) {
		l.Log().WithFields(logrus.Fields{
			l.From: "new-postgres-backend",
		}).Warnf("PostgreSQL backend not configured, skipping")

		return b, false
	} else if err != nil {
		l.Log().WithFields(logrus.Fields{
			l.From: "new-postgres-backend",
		}).Errorf("unable to create PostgreSQL backend: %s", err.Error())

		return b, false
	}

	return b, true
}

func (bf BackendRegistryFactory) newFileBackend(ctx context.Context) BackendMissionRegistry {
	return file.NewClient(ctx)
}

func New() (*Storage, error) {
	backend := BackendRegistryFactory{}.New()

	return &Storage{
		Backend: backend,
	}, nil
}

func (s *Storage) teardown(ctx context.Context) {
	l.Log().WithField(l.From, "storage-teardown").Trace("teardown called")
	s.Backend.Close(ctx)
	l.Log().WithField(l.From, "storage-teardown").Trace("teardown done")
}

func (s Storage) Marshall(d map[string]interface{}) ([]byte, error) {
	return json.Marshal(d)
}

func (s Storage) Unmarshall(content []byte, d *map[string]interface{}) error {
	return json.Unmarshal(content, d)
}

func (s Storage) String() string {
	return "storage"
}

func (s Storage) Start(ctx context.Context, wg *sync.WaitGroup, ipc *threads.IPC) error {
	defer wg.Done()

	ipcRecvChan, err := ipc.Get(threads.StorageChannel)
	if err != nil {
		return fmt.Errorf("unable to get storage channel: %w", err)
	}

	for {
		select {
		case msg := <-ipcRecvChan:
			var data map[string]interface{}
			if err := s.Unmarshall(msg.Content, &data); err != nil {
				l.Log().WithField(l.From, "storage").Errorf("unable to unmarshall frame: %s", err.Error())
				continue
			}

			if err := s.Backend.Save(ctx, data); err != nil {
				l.Log().WithField(l.From, "storage").Errorf("unable to save data: %s", err.Error())
				continue
			}
			l.Log().WithField(l.From, "storage").Trace("saved data")
		case <-ctx.Done():
			l.Log().WithField(l.From, "storage").Info("cancelled, closing")

			cctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			s.teardown(cctx)
			return nil
		}
	}
}
