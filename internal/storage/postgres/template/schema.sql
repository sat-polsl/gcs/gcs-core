CREATE TABLE IF NOT EXISTS spacecraft_1 (
    _id SERIAL,
    timestamp DOUBLE PRECISION,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    altitude DOUBLE PRECISION,
    spacecraft_id INT
);
