CREATE TABLE IF NOT EXISTS missions_registry(
    _id SERIAL,
    _creation_date TIMESTAMP DEFAULT NOW(),
    _columns VARCHAR(4096),
    mission_name VARCHAR(128),
    mission_description VARCHAR(1024)
);