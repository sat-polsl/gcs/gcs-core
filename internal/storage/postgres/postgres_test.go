package postgres_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	// embed is used for loading template/schema.sql
	_ "embed"

	"github.com/jackc/pgx/v4"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/postgres"
	"gitlab.com/sat-polsl/gcs/gcs-core/internal/tools"
)

const (
	TestUsername       = "test_user"
	TestPassword       = "test_password"
	TestDB             = "sat_test"
	ConnectionString   = "postgresql://localhost:%s/%s?user=%s&password=%s"
	ConnectionTemplate = "postgresql://{{.Host}}:{{.Port}}/{{.Database}}?user={{.Username}}&password={{.Password}}"
)

var (
	//go:embed template/schema.sql
	SQLTable string

	Port string
	URL  string

	data = map[string]interface{}{
		"spacecraft_id": 1.0,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}
	enumerated = "$1, $2, $3, $4, $5"
)

func wrap(f func(context.Context) error, ctx context.Context, t *testing.T) {
	if err := f(ctx); err != nil {
		t.Errorf("error during wrap function: %s", err.Error())
	}
}

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	// pull mongodb docker image for version 5.0
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "latest",
		Env: []string{
			tools.KeyValue{Key: "POSTGRES_USER", Value: TestUsername}.String(),
			tools.KeyValue{Key: "POSTGRES_PASSWORD", Value: TestPassword}.String(),
			tools.KeyValue{Key: "POSTGRES_DB", Value: TestDB}.String(),
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	err = pool.Retry(func() error {
		Port = resource.GetPort("5432/tcp")
		URL = fmt.Sprintf(ConnectionString, Port, TestDB, TestUsername, TestPassword)

		dbClient, err := pgx.Connect(context.TODO(), URL)
		if err != nil {
			return fmt.Errorf("unable to connect to database (%s): %w", URL, err)
		}
		defer dbClient.Close(context.TODO())

		if err := dbClient.Ping(context.TODO()); err != nil {
			return fmt.Errorf("unable to ping: %w", err)
		}

		tx, err := dbClient.Begin(context.TODO())
		if err != nil {
			return fmt.Errorf("unable to begin transaction: %w", err)
		}
		defer tx.Rollback(context.TODO())

		if _, err := tx.Exec(context.TODO(), SQLTable); err != nil {
			return fmt.Errorf("unable to execute statement: %w", err)
		}

		if err = tx.Commit(context.TODO()); err != nil {
			return fmt.Errorf("unable to commit transaction: %w", err)
		}

		return nil
	})
	if err != nil {
		err = fmt.Errorf("Could not connect to docker: %w", err)
		if err2 := pool.Purge(resource); err2 != nil {
			log.Fatalf("Could not purge resource: %s, caused after: %s", err2.Error(), err.Error())
		}
		log.Fatalf(err.Error())
	}

	os.Setenv(postgres.EnvPostgresUsername, TestUsername)
	os.Setenv(postgres.EnvPostgresPassword, TestPassword)
	os.Setenv(postgres.EnvPostgresDB, TestDB)
	os.Setenv(postgres.EnvPostgresHost, "localhost")
	os.Setenv(postgres.EnvPostgresPort, Port)

	// run tests
	code := m.Run()

	// When you're done, kill and remove the container
	if err = pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestDivide(t *testing.T) {
	k, v := postgres.Divide(data)

	if len(k) != len(v) {
		t.Errorf("length of keys (%d) is different of length of values (%d)", len(k), len(v))
	}
}

func TestEnumerate(t *testing.T) {
	_, v := postgres.Divide(data)
	e := postgres.Enumerate(v)

	if e != enumerated {
		t.Errorf("got `%s`, wanted `%s`", e, enumerated)
	}
}

func TestBuildInsert(t *testing.T) {
	ins, v, err := postgres.BuildInsert(data)
	if err != nil {
		t.Errorf("failed to build insert: %s", err.Error())
	}

	if len(data) != len(v) {
		t.Errorf("length of data (%d) is different of length of values (%d)", len(data), len(v))
	}

	if len(ins) < 1 {
		t.Errorf("wrong length of insert: `%s`", ins)
	}
}

func TestSave(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx)
	if err != nil {
		t.Errorf("failed to create postgres client: %s", err.Error())
		return
	}

	defer wrap(c.Close, ctx, t)

	if err = c.Save(ctx, data); err != nil {
		t.Errorf("error during saving data: %s", err.Error())
	}
}
