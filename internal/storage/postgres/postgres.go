package postgres

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"html/template"
	"os"
	"strings"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jeremywohl/flatten"
	"github.com/sirupsen/logrus"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage/vault"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

type PostgresClient struct {
	Client *pgxpool.Pool
}

type Config struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}

func NewConfig() (Config, error) {
	var username, password string

	vaultToken := os.Getenv(vault.EnvVaultToken)
	if len(vaultToken) != 0 {
		dc, err := vault.NewDynamicCredentials(os.Getenv(vault.EnvVaultToken))
		if err != nil {
			return Config{}, fmt.Errorf("unable to create dynamic credentials: %w", err)
		}

		username, password, err = dc.Get("postgresdb")
		if err != nil {
			return Config{}, fmt.Errorf("unable to retrieve credentials: %w", err)
		}
	} else {
		username = os.Getenv(EnvPostgresUsername)
		if len(username) == 0 {
			return Config{}, ErrNotConfigured{What: EnvPostgresUsername}
		}

		password = os.Getenv(EnvPostgresPassword)
		if len(password) == 0 {
			return Config{}, ErrNotConfigured{What: EnvPostgresUsername}
		}
	}

	host := os.Getenv(EnvPostgresHost)
	if len(host) == 0 {
		host = DefaultPostgresHost
		l.Log().WithFields(logrus.Fields{
			EnvPostgresHost: host,
			l.From:          "new-postgres-client",
		}).Warn("postgres database not set, falling back to default")
	}

	db := os.Getenv(EnvPostgresDB)
	if len(db) == 0 {
		db = DefaultPostgresDB
		l.Log().WithFields(logrus.Fields{
			EnvPostgresDB: db,
			l.From:        "new-postgres-client",
		}).Warn("postgres database not set, falling back to default")
	}

	port := os.Getenv(EnvPostgresPort)
	if len(port) == 0 {
		port = DefaultPostgresPort
		l.Log().WithFields(logrus.Fields{
			EnvPostgresPort: port,
			l.From:          "new-postgres-client",
		}).Warn("postgres port not set, falling back to default")
	}

	return Config{
		Host:     host,
		Port:     port,
		Database: db,
		Username: username,
		Password: password,
	}, nil
}

func New(ctx context.Context) (PostgresClient, error) {
	connTpl, err := template.New("connection_string").Parse(ConnectionString)
	if err != nil {
		return PostgresClient{}, fmt.Errorf("unable to create template from connection string: %w", err)
	}

	cfg, err := NewConfig()
	if err != nil {
		return PostgresClient{}, fmt.Errorf("unable to create new configuration: %w", err)
	}

	var bytesTpl bytes.Buffer
	connTpl.Execute(&bytesTpl, cfg)
	l.Log().WithFields(logrus.Fields{
		"connection_string": bytesTpl.String(),
		l.From:              "new-postgres-client",
	}).Traceln("connection string created")

	client, err := pgxpool.Connect(ctx, bytesTpl.String())
	if err != nil {
		return PostgresClient{}, fmt.Errorf("unable to connect to PostgreSQL (%s): %w", bytesTpl.String(), err)
	}

	tx, err := client.Begin(ctx)
	if err != nil {
		return PostgresClient{}, fmt.Errorf("unable to begin transaction: %w", err)
	}
	defer tx.Rollback(ctx)

	if _, err := tx.Exec(ctx, MissionsRegistryCreateSQL); err != nil {
		return PostgresClient{}, fmt.Errorf("unable to ensure existence of Missions Registry: %w", err)
	}

	if err := tx.Commit(ctx); err != nil {
		return PostgresClient{}, fmt.Errorf("unable to commit transaction: %w", err)
	}

	return PostgresClient{
		Client: client,
	}, nil
}

func BuildInsert(data map[string]interface{}) (string, []interface{}, error) {
	tplStr := `INSERT INTO spacecraft_%d(%s) VALUES (%s) RETURNING _id;`

	tableName := int(data["spacecraft_id"].(float64))

	flatData, err := flatten.Flatten(data, "", flatten.UnderscoreStyle)
	if err != nil {
		return "", nil, fmt.Errorf("unable to flatten document: %w", err)
	}

	columns, values := Divide(flatData)

	insert := fmt.Sprintf(tplStr, tableName, strings.Join(columns, ", "), Enumerate(values))

	return insert, values, nil
}

func BuildCreate(name string, columns map[string]string) (string, error) {
	if len(columns) < 1 {
		return "", ErrNoColumns
	}

	columnsStr := ""
	for name, sqltype := range columns {
		columnsStr += fmt.Sprintf(",\n %s %v", name, sqltype)
	}

	return fmt.Sprintf(MissionCreateSQL, name, columnsStr), nil
}

func Divide(flatData map[string]interface{}) (columns []string, values []interface{}) {
	for k, v := range flatData {
		columns = append(columns, k)
		values = append(values, v)
	}

	return
}

func Enumerate(d []interface{}) string {
	str := ""
	for i, _ := range d {
		if i == 0 {
			str = fmt.Sprintf("$%d", i+1)
		} else {
			str += fmt.Sprintf(", $%d", i+1)
		}
	}

	return str
}

func (p PostgresClient) CreateMission(ctx context.Context, name string, description string, columns map[string]string) error {
	tx, err := p.Client.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}
	defer tx.Rollback(ctx)

	SQLCreate, err := BuildCreate(name, columns)
	if err != nil {
		return fmt.Errorf("unable to build create statement: %w", err)
	}

	if _, err := tx.Exec(ctx, SQLCreate); err != nil {
		return fmt.Errorf("unable to execute create statement: %w", err)
	}

	var columnNames []string
	for name := range columns {
		columnNames = append(columnNames, name)
	}

	if _, err := tx.Exec(ctx, MissionsRegistryInsertSQL, name, description, strings.Join(columnNames, ",")); err != nil {
		return fmt.Errorf("unable to insert into mission registry: %w", err)
	}

	return tx.Commit(ctx)
}

func (p PostgresClient) UpdateMission(ctx context.Context, name string, description string) error {
	tx, err := p.Client.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}
	defer tx.Rollback(ctx)

	if _, err := tx.Exec(ctx, MissionsRegistryUpdateSQL, name, description); err != nil {
		return fmt.Errorf("unable to execute update statement: %w", err)
	}

	return tx.Commit(ctx)
}

func (p PostgresClient) ListMissions(ctx context.Context) (out []map[string]string, err error) {
	rows, err := p.Client.Query(ctx, MissionsRegistrySelectSQL)
	if err != nil {
		return out, fmt.Errorf("unable to select missions: %w", err)
	}

	for rows.Next() {
		var name, description string
		err = rows.Scan(&name, &description)
		if err != nil {
			return out, fmt.Errorf("error during scanning: %w", err)
		}

		out = append(out, map[string]string{
			"name":        name,
			"description": description,
		})
	}

	if rows.Err() != nil {
		return out, fmt.Errorf("during reading an error had occured: %w", rows.Err())
	}

	return out, nil
}

func (p PostgresClient) GetValues(ctx context.Context, id string) (out []map[string]interface{}, err error) {
	rows, err := p.Client.Query(ctx, MissionsRegistryGetColumnsSQL, id)
	if err != nil {
		return out, fmt.Errorf("unable to select missions: %w", err)
	}

	var columns sql.NullString
	for rows.Next() {
		err = rows.Scan(&columns)
		if err != nil {
			return out, fmt.Errorf("error during scanning: %w", err)
		}
	}

	if rows.Err() != nil {
		return out, fmt.Errorf("during reading an error had occured: %w", rows.Err())
	}

	rows, err = p.Client.Query(ctx, fmt.Sprintf(MissionSelectSQL, columns.String, id))
	if err != nil {
		return out, fmt.Errorf("unable to select missions: %w", err)
	}

	for rows.Next() {
		rowValues := map[string]interface{}{}
		row, err := rows.Values()
		if err != nil {
			return out, fmt.Errorf("error during scanning: %w", err)
		}

		for i, s := range strings.Split(columns.String, ",") {
			rowValues[s] = row[i]
		}

		out = append(out, rowValues)
	}

	if rows.Err() != nil {
		return out, fmt.Errorf("during reading an error had occured: %w", rows.Err())
	}

	return out, nil
}

func (p PostgresClient) MissionInfo(ctx context.Context, id string) (map[string]interface{}, error) {
	var (
		records      sql.NullInt64
		newestRecord sql.NullString
		oldestRecord sql.NullString
		name         sql.NullString
		description  sql.NullString
	)

	rows := p.Client.QueryRow(ctx, fmt.Sprintf(MissionInfoSQL, id))
	if err := rows.Scan(&records, &newestRecord, &oldestRecord); err != nil && err != pgx.ErrNoRows {
		return nil, fmt.Errorf("unable to select missions: %w", err)
	}

	rows = p.Client.QueryRow(ctx, fmt.Sprintf(MissionsRegistrySelectOneSQL, id))
	if err := rows.Scan(&name, &description); err != nil && err != pgx.ErrNoRows {
		return nil, fmt.Errorf("unable to select missions: %w", err)
	}

	return map[string]interface{}{
		"name":        name.String,
		"description": description.String,
		"records":     records.Int64,
		"oldest":      oldestRecord.String,
		"newest":      newestRecord.String,
	}, nil
}

func (p PostgresClient) Save(ctx context.Context, data map[string]interface{}) error {
	insert, values, err := BuildInsert(data)
	if err != nil {
		return fmt.Errorf("unable to build insert: %w", err)
	}

	var id int64
	tx, err := p.Client.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}
	defer tx.Rollback(ctx)

	tx.QueryRow(ctx, insert, values...).Scan(&id)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}

	return tx.Commit(ctx)
}

func (p PostgresClient) Close(ctx context.Context) error {
	p.Client.Close()
	return nil
}
