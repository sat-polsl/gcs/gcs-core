package postgres

import (
	"fmt"

	// embed is used for template inclusion
	_ "embed"
)

// Constants containing names of ENV variables
const (
	EnvPostgresHost     = "POSTGRES_HOST"
	EnvPostgresPort     = "POSTGRES_PORT"
	EnvPostgresDB       = "POSTGRES_DATABASE"
	EnvPostgresUsername = "POSTGRES_USERNAME"
	EnvPostgresPassword = "POSTGRES_PASSWORD"
)

const (
	ConnectionString    = `postgresql://{{.Host}}:{{.Port}}/{{.Database}}?user={{.Username}}&password={{.Password}}`
	DefaultPostgresDB   = "sat_dev"
	DefaultPostgresHost = "postgres"
	DefaultPostgresPort = "5432"
)

var (
	ErrNoColumns = fmt.Errorf("no columns for the table")
)

var (
	//go:embed template/mission/create.sql
	MissionCreateSQL string
	//go:embed template/mission/info.sql
	MissionInfoSQL string
	//go:embed template/mission/select.sql
	MissionSelectSQL string

	//go:embed template/missions_registry/create.sql
	MissionsRegistryCreateSQL string
	//go:embed template/missions_registry/insert.sql
	MissionsRegistryInsertSQL string
	//go:embed template/missions_registry/update.sql
	MissionsRegistryUpdateSQL string
	//go:embed template/missions_registry/select.sql
	MissionsRegistrySelectSQL string
	//go:embed template/missions_registry/get_columns.sql
	MissionsRegistryGetColumnsSQL string
	//go:embed template/missions_registry/select_one.sql
	MissionsRegistrySelectOneSQL string
)

type ErrNotConfigured struct {
	What string
}

func (err ErrNotConfigured) Error() string {
	return fmt.Sprintf("%s not configured", err.What)
}

func (err ErrNotConfigured) Is(target error) bool {
	return target == ErrNotConfigured{}
}
