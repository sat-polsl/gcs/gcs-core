package storage_test

import (
	"context"
	"testing"

	"gitlab.com/sat-polsl/gcs/gcs-core/internal/storage"
)

func TestFactory(t *testing.T) {
	smoke := storage.BackendRegistryFactory{}.New()

	err := smoke.Close(context.TODO())
	if err != nil {
		t.Errorf("smoke test failed: %s", err.Error())
	}
}
