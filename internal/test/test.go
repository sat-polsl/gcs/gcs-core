package test

import (
	"testing"
)

func Assert[T comparable](t *testing.T, a T, b T) {
	if a != b {
		t.Errorf("a and b have different values: a: %v, b: %v", a, b)
	}
}
