package threads

import "fmt"

// Constants containing names of env variables
const (
	StorageChannel             = "storage"
	SubRotorChannel            = "rx_frame"
	PubRotorChanel             = "rotor_ctl"
	CriticalNotificationChanel = "crit_error"
)

type ErrChannelAlreadyExists struct {
	ChannelName string
}

func (err ErrChannelAlreadyExists) Error() string {
	return fmt.Sprintf("channe %s already exists", err.ChannelName)
}

type ErrChannelNotFound struct {
	ChannelName string
}

func (err ErrChannelNotFound) Error() string {
	return fmt.Sprintf("channe %s does not exist", err.ChannelName)
}
