package threads

import (
	"context"
	"sync"
)

type Message struct {
	Topic   string
	Content []byte
}

func NewMessage(b [][]byte) Message {
	if len(b) >= 2 {
		return Message{
			Topic:   string(b[0]),
			Content: b[1],
		}
	} else if len(b) == 1 {
		return Message{
			Topic: string(b[0]),
		}
	}

	return Message{}
}

type FrameConsumer interface {
	Unmarshall()
}

type FrameProducer interface {
	Marshall()
}

type FrameProducerConsumer interface {
	Unmarshall()
	Marshall()
}

type Starter interface {
	Start(context.Context, *sync.WaitGroup, *IPC) error
	String() string
}
