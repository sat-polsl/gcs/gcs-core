package threads

import (
	"sync"
)

type IPC struct {
	mutex    sync.RWMutex
	chanMap  map[string]chan Message
	channels []string
}

func New() *IPC {
	return &IPC{
		chanMap:  make(map[string]chan Message),
		channels: []string{},
	}
}

func (ipc *IPC) Add(name string, c chan Message) error {
	if ipc.exists(name) {
		return ErrChannelAlreadyExists{
			ChannelName: name,
		}
	}

	ipc.add(name, c)

	return nil
}

func (ipc *IPC) New(name string) (chan Message, error) {
	if ipc.exists(name) {
		return nil, ErrChannelAlreadyExists{
			ChannelName: name,
		}
	}

	ipc.add(name, make(chan Message))

	return ipc.get(name), nil
}

func (ipc *IPC) Get(name string) (chan Message, error) {
	if !ipc.exists(name) {
		return nil, ErrChannelNotFound{
			ChannelName: name,
		}
	}

	return ipc.get(name), nil
}

func (ipc *IPC) exists(name string) bool {
	for _, v := range ipc.channels {
		if name == v {
			return true
		}
	}

	return false
}

func (ipc *IPC) add(name string, c chan Message) {
	ipc.mutex.Lock()
	defer ipc.mutex.Unlock()

	ipc.channels = append(ipc.channels, name)
	ipc.chanMap[name] = c
}

func (ipc *IPC) get(name string) chan Message {
	ipc.mutex.RLock()
	defer ipc.mutex.RUnlock()

	return ipc.chanMap[name]
}
