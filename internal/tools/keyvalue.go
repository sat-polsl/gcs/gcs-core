package tools

import "fmt"

type KeyValue struct {
	Key   string
	Value string
}

func (kv KeyValue) String() string {
	return fmt.Sprintf("%s=%s", kv.Key, kv.Value)
}
