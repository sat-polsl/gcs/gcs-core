// Package tools is a collection of tool functions that are used in the server
// and api packages.
package tools

import (
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	l "gitlab.com/sat-polsl/gcs/gcs-core/logger"
)

func ErrContains(err error, s string) bool {
	return strings.Contains(err.Error(), s)
}

type ResponseMap map[string]interface{}

func Shard(data [][]byte, maxSize string) [][]byte {
	var sharded [][]byte
	return sharded
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func WriteResponse(w http.ResponseWriter, r *http.Request, statusCode int, res ResponseMap) {
	b, err := json.MarshalIndent(res, "", "    ")
	if err != nil {
		WriteError(w, r, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(statusCode)
	w.Write(b)
}

func WriteCSV(w http.ResponseWriter, r *http.Request, statusCode int, res string) {
	w.WriteHeader(statusCode)
	w.Write([]byte(res))
}

func WriteError(w http.ResponseWriter, r *http.Request, statusCode int, err error) {
	l.Log().WithField(l.At, r.URL).Error(err.Error())

	b, err := json.MarshalIndent(ResponseMap{
		"error": err.Error(),
	}, "", "    ")
	if err != nil {
		l.Log().WithFields(logrus.Fields{
			l.At:   r.URL,
			l.From: "write-error",
		}).Error(err.Error())
		return
	}

	w.WriteHeader(statusCode)
	w.Write(b)
}

func RegisterGet(mux *mux.Router, route string, handle func(http.ResponseWriter, *http.Request)) {
	mux.HandleFunc(route, handle).Methods(http.MethodGet)
	mux.HandleFunc(route+"/", handle).Methods(http.MethodGet)
}

func RegisterPost(mux *mux.Router, route string, handle func(http.ResponseWriter, *http.Request)) {
	mux.HandleFunc(route, handle).Methods(http.MethodPost)
	mux.HandleFunc(route+"/", handle).Methods(http.MethodPost)
}

func RegisterPatch(mux *mux.Router, route string, handle func(http.ResponseWriter, *http.Request)) {
	mux.HandleFunc(route, handle).Methods(http.MethodPatch)
	mux.HandleFunc(route+"/", handle).Methods(http.MethodPatch)
}

func RegisterPut(mux *mux.Router, route string, handle func(http.ResponseWriter, *http.Request)) {
	mux.HandleFunc(route, handle).Methods(http.MethodPut)
	mux.HandleFunc(route+"/", handle).Methods(http.MethodPut)
}

func RegisterDelete(mux *mux.Router, route string, handle func(http.ResponseWriter, *http.Request)) {
	mux.HandleFunc(route, handle).Methods(http.MethodDelete)
	mux.HandleFunc(route+"/", handle).Methods(http.MethodDelete)
}

type EnvVar map[string]string

func SetEnvs(envs map[string]string) error {
	for k, v := range envs {
		if err := os.Setenv(k, v); err != nil {
			return err
		}
	}

	return nil
}

func SkipTestOnCI(t *testing.T) {
	CI := os.Getenv("CI_NO_DOCKER")
	if len(CI) != 0 {
		if ci, err := strconv.ParseBool(CI); err != nil {
			t.Errorf("unable to parse bool during skipping the task: %s", err.Error())
		} else if !ci {
			t.Skip("running inside CI, this test will be skipped")
		}
	}
}
